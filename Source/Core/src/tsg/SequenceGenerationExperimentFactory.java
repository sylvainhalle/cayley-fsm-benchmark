/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg;

import ca.uqac.lif.ecp.Edge;
import ca.uqac.lif.ecp.atomic.ActionShallowHistory;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.ecp.atomic.AutomatonCayleyGraphFactory;
import ca.uqac.lif.ecp.atomic.GffAutomatonParser;
import ca.uqac.lif.ecp.atomic.StateShallowHistory;
import ca.uqac.lif.ecp.atomic.TransitionShallowHistory;
import ca.uqac.lif.labpal.ExperimentFactory;
import ca.uqac.lif.labpal.Random;
import ca.uqac.lif.labpal.Region;
import ca.uqac.lif.structures.MathList;
import tsg.cayleygraph.CayleyClosureSequenceGenerationExperiment;
import tsg.cayleygraph.CayleySequenceGenerationExperiment;
import tsg.direct.DirectSequenceGenerationExperiment;
import tsg.direct.LineGraphSequenceGenerationExperiment;
import tsg.direct.SpanningTreeSequenceGenerationExperiment;
import tsg.graphwalker.GraphWalkerSequenceGenerationExperiment;
import tsg.greedy.GreedyClosureSequenceGenerationExperiment;
import tsg.greedy.GreedySequenceGenerationExperiment;

import static tsg.SequenceGenerationExperiment.ALGORITHM;
import static tsg.SequenceGenerationExperiment.CLOSURE;
import static tsg.SequenceGenerationExperiment.CLOSURE_HYPERGRAPH;
import static tsg.SequenceGenerationExperiment.FUNCTION;
import static tsg.SequenceGenerationExperiment.SPECIFICATION;
import static tsg.SequenceGenerationExperiment.STRENGTH;

import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;

/**
 * Instantiates a {@link SequenceGenerationExperiment} based on parameters
 * specified by a {@link Region}.
 */
@SuppressWarnings("rawtypes")
public class SequenceGenerationExperimentFactory extends ExperimentFactory<MyLaboratory,SequenceGenerationExperiment>
{
	/**
	 * The name of the state coverage triaging function
	 */
	public static final transient String STATE_COVERAGE = "State coverage";

	/**
	 * The name of the state residual triaging function
	 */
	public static final transient String STATE_RESIDUAL = "State residual";
	
	/**
	 * The name of the action coverage triaging function
	 */
	public static final transient String ACTION_COVERAGE = "Action coverage";
	
	/**
	 * The name of the action residual triaging function
	 */
	public static final transient String ACTION_RESIDUAL = "Action residual";
	
	/**
	 * The name of the transition coverage triaging function
	 */
	public static final transient String TRANSITION_COVERAGE = "Transition coverage";
	
	/**
	 * The name of the transition residual triaging function
	 */
	public static final transient String TRANSITION_RESIDUAL = "Transition residual";
	
	/**
	 * The name of the t-way state coverage triaging function
	 */
	public static final transient String TWAY_STATE = "T-way state coverage";
	
	/**
	 * The name of the t-way action coverage triaging function
	 */
	public static final transient String TWAY_ACTION = "T-way action coverage";
	
	/**
	 * The name of the t-way transition coverage triaging function
	 */
	public static final transient String TWAY_TRANSITION = "T-way transition coverage";
	
	/**
	 * The random source used in the lab.
	 */
	protected Random m_random;

	/**
	 * A map associating specification names to the filename that contains
	 * them inside the lab
	 */
	protected Map<String,String> m_specifications;
	
	/**
	 * A parser for automata in GFF (i.e. XML) format. These specifications come
	 * from the <a href="http://buchi.im.ntu.edu.tw">Büchi Store</a>.
	 */
	protected static final transient GffAutomatonParser s_gffParser = new GffAutomatonParser();
	
	/**
	 * Creates a new instance of the factory
	 * @param lab The lab to which the experiments will be added
	 */
	public SequenceGenerationExperimentFactory(MyLaboratory lab) 
	{
		super(lab, SequenceGenerationExperiment.class);
		m_specifications = new HashMap<String,String>();
		m_random = lab.getRandom();
	}

	/**
	 * Adds new specifications to the factory
	 * @param scenarios A map associating specification names to the
	 * filename that contains them inside the lab 
	 */
	public void add(Map<String,String> scenarios)
	{
		m_specifications.putAll(scenarios);
	}

	/**
	 * Gets the names of all specifications registered in this factory
	 * @return A set of specification names
	 */
	public Set<String> getSpecificationNames()
	{
		return m_specifications.keySet();
	}

	/**
	 * Gets the experiment associated to the region
	 * @param r A region with parameters defining the experiment
	 * @return The experiment, or <tt>null</tt> if no experiment could be
	 * generated
	 */
	@Override
	public SequenceGenerationExperiment<?> createExperiment(Region r)
	{
		String algo_name = r.getString(ALGORITHM);
		String triaging_function = r.getString(FUNCTION);
		String closure = r.getString(CLOSURE);
		if (algo_name == null || triaging_function == null)
		{
			return null;
		}
		if (triaging_function.compareTo(STATE_COVERAGE) == 0)
		{
			if (closure.compareTo(CLOSURE_HYPERGRAPH) == 0)
			{
				return getStateCoverageExperiment(algo_name, r);
			}
			else
			{
				return getStateCoverageClosureExperiment(algo_name, r);
			}
		}
		if (triaging_function.compareTo(STATE_RESIDUAL) == 0)
		{
			return getStateResidualExperiment(algo_name, r);
		}
		if (triaging_function.compareTo(ACTION_COVERAGE) == 0)
		{
			if (closure.compareTo(CLOSURE_HYPERGRAPH) == 0)
			{
				return getActionCoverageExperiment(algo_name, r);
			}
			else
			{
				return getActionCoverageClosureExperiment(algo_name, r);
			}
		}
		if (triaging_function.compareTo(ACTION_RESIDUAL) == 0)
		{
			return getActionResidualExperiment(algo_name, r);
		}
		if (triaging_function.compareTo(TRANSITION_COVERAGE) == 0)
		{
			if (closure.compareTo(CLOSURE_HYPERGRAPH) == 0)
			{
				return getTransitionCoverageExperiment(algo_name, r);
			}
			else
			{
				return getTransitionCoverageClosureExperiment(algo_name, r);
			}
		}
		if (triaging_function.compareTo(TRANSITION_RESIDUAL) == 0)
		{
			return getTransitionResidualExperiment(algo_name, r);
		}
		// The next functions require an additional parameter
		int strength = 1;
		if (!r.hasDimension(STRENGTH))
		{
			return null;
		}
		strength = r.getInt(STRENGTH);
		if (triaging_function.compareTo(TWAY_STATE) == 0)
		{
			return getTWayStateCoverageExperiment(algo_name, strength, r);
		}
		if (triaging_function.compareTo(TWAY_TRANSITION) == 0)
		{
			return getTWayTransitionCoverageExperiment(algo_name, strength, r);
		}
		if (triaging_function.compareTo(TWAY_ACTION) == 0)
		{
			return getTWayActionCoverageExperiment(algo_name, strength, r);
		}
		return null;
	}
	
	/**
	 * Produces an empty instance of a {@link SequenceGenerationExperiment}
	 * depending on the algorithm used to generate the sequence.
	 * @param algo_name The name of the algorithm
	 * @return An instance of experiment
	 */
	protected SequenceGenerationExperiment<MathList<Integer>> getExperimentInstance(String algo_name)
	{
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			return new CayleySequenceGenerationExperiment<MathList<Integer>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			return new GreedySequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
		}
		else if (algo_name.compareTo(GraphWalkerSequenceGenerationExperiment.NAME) == 0)
		{
			return new GraphWalkerSequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
		}
		return null;
	}

	/**
	 * Generates an experiment for the state coverage triaging function.
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getStateCoverageExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<Integer>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleySequenceGenerationExperiment<MathList<Integer>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedySequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
			((GreedySequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(GraphWalkerSequenceGenerationExperiment.NAME) == 0)
		{
			e = new GraphWalkerSequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
			((GraphWalkerSequenceGenerationExperiment<?>) e).setGeneratorString("quick_random(vertex_coverage(100))");
			((GraphWalkerSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(DirectSequenceGenerationExperiment.NAME) == 0)
		{
			e = new SpanningTreeSequenceGenerationExperiment();
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(true);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new StateShallowHistory(a, 1), STATE_COVERAGE);
		AutomatonCayleyGraphFactory<MathList<Integer>> factory = new AutomatonCayleyGraphFactory<MathList<Integer>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the state coverage triaging using the alternate
	 * construction for prefix closure.
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getStateCoverageClosureExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<Integer>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleyClosureSequenceGenerationExperiment<MathList<Integer>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedyClosureSequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
			((GreedyClosureSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(GraphWalkerSequenceGenerationExperiment.NAME) == 0)
		{
			e = new GraphWalkerSequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
			((GraphWalkerSequenceGenerationExperiment<?>) e).setGeneratorString("quick_random(vertex_coverage(100))");
			((GraphWalkerSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(DirectSequenceGenerationExperiment.NAME) == 0)
		{
			e = new SpanningTreeSequenceGenerationExperiment();
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new StateShallowHistory(a, 1), STATE_COVERAGE);
		AutomatonCayleyGraphFactory<MathList<Integer>> factory = new AutomatonCayleyGraphFactory<MathList<Integer>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}

	/**
	 * Generates an experiment for the state residual triaging function.
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getStateResidualExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<Integer>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleySequenceGenerationExperiment<MathList<Integer>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedySequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
			((GreedySequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new StateShallowHistory(a, 1), STATE_RESIDUAL);
		AutomatonCayleyGraphFactory<MathList<Integer>> factory = new AutomatonCayleyGraphFactory<MathList<Integer>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the action residual triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getActionResidualExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<AtomicEvent>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleySequenceGenerationExperiment<MathList<AtomicEvent>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedySequenceGenerationExperiment<MathList<AtomicEvent>>().setRandom(m_random);
			((GreedySequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new ActionShallowHistory(a, 1), ACTION_RESIDUAL);
		AutomatonCayleyGraphFactory<MathList<AtomicEvent>> factory = new AutomatonCayleyGraphFactory<MathList<AtomicEvent>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the action coverage triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getActionCoverageExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<AtomicEvent>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleySequenceGenerationExperiment<MathList<AtomicEvent>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedySequenceGenerationExperiment<MathList<AtomicEvent>>().setRandom(m_random);
			((GreedySequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(GraphWalkerSequenceGenerationExperiment.NAME) == 0)
		{
			e = new GraphWalkerSequenceGenerationExperiment<MathList<AtomicEvent>>().setRandom(m_random);
			((GraphWalkerSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(true);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new ActionShallowHistory(a, 1), ACTION_COVERAGE);
		AutomatonCayleyGraphFactory<MathList<AtomicEvent>> factory = new AutomatonCayleyGraphFactory<MathList<AtomicEvent>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the action coverage triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getActionCoverageClosureExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<AtomicEvent>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleyClosureSequenceGenerationExperiment<MathList<AtomicEvent>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedyClosureSequenceGenerationExperiment<MathList<AtomicEvent>>().setRandom(m_random);
			((GreedyClosureSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new ActionShallowHistory(a, 1), ACTION_COVERAGE);
		AutomatonCayleyGraphFactory<MathList<AtomicEvent>> factory = new AutomatonCayleyGraphFactory<MathList<AtomicEvent>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the transition residual triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getTransitionResidualExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<Edge<AtomicEvent>>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleySequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedySequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>().setRandom(m_random);
			((GreedySequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new TransitionShallowHistory(a, 1), TRANSITION_RESIDUAL);
		AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>> factory = new AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the transition coverage triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getTransitionCoverageExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<Edge<AtomicEvent>>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleySequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedySequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>().setRandom(m_random);
			((GreedySequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(GraphWalkerSequenceGenerationExperiment.NAME) == 0)
		{
			e = new GraphWalkerSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>().setRandom(m_random);
			((GraphWalkerSequenceGenerationExperiment<?>) e).setGeneratorString("quick_random(edge_coverage(100))");
			//((GraphWalkerSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(DirectSequenceGenerationExperiment.NAME) == 0)
		{
			e = new LineGraphSequenceGenerationExperiment();
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(true);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new TransitionShallowHistory(a, 1), TRANSITION_COVERAGE);
		AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>> factory = new AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the transition coverage triaging function
	 * using the alternate construction for prefix closure.
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getTransitionCoverageClosureExperiment(String algo_name, Region r)
	{
		SequenceGenerationExperiment<MathList<Edge<AtomicEvent>>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleyClosureSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedyClosureSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>().setRandom(m_random);
			((GreedyClosureSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(GraphWalkerSequenceGenerationExperiment.NAME) == 0)
		{
			e = new GraphWalkerSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>().setRandom(m_random);
			((GraphWalkerSequenceGenerationExperiment<?>) e).setGeneratorString("quick_random(edge_coverage(100))");
			//((GraphWalkerSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		else if (algo_name.compareTo(DirectSequenceGenerationExperiment.NAME) == 0)
		{
			e = new LineGraphSequenceGenerationExperiment();
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new TransitionShallowHistory(a, 1), TRANSITION_COVERAGE);
		AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>> factory = new AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the <i>t</i>-way state coverage triaging function.
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param t The strength of the coverage (i.e. the value of parameter <i>t</i>)
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getTWayStateCoverageExperiment(String algo_name, int t, Region r)
	{
		SequenceGenerationExperiment<MathList<Integer>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleyClosureSequenceGenerationExperiment<MathList<Integer>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedyClosureSequenceGenerationExperiment<MathList<Integer>>().setRandom(m_random);
			((GreedyClosureSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new StateShallowHistory(a, t), TWAY_STATE);
		e.setStrength(t);
		AutomatonCayleyGraphFactory<MathList<Integer>> factory = new AutomatonCayleyGraphFactory<MathList<Integer>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the <i>t</i>-way action coverage triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param t The strength of the coverage (i.e. the value of parameter <i>t</i>)
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getTWayActionCoverageExperiment(String algo_name, int t, Region r)
	{
		SequenceGenerationExperiment<MathList<AtomicEvent>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleyClosureSequenceGenerationExperiment<MathList<AtomicEvent>>();
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedyClosureSequenceGenerationExperiment<MathList<AtomicEvent>>().setRandom(m_random);
			((GreedyClosureSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new ActionShallowHistory(a, t), TWAY_ACTION);
		e.setStrength(t);
		AutomatonCayleyGraphFactory<MathList<AtomicEvent>> factory = new AutomatonCayleyGraphFactory<MathList<AtomicEvent>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}
	
	/**
	 * Generates an experiment for the <i>t</i>-way transition coverage triaging function
	 * @param algo_name The name of the algorithm used to generate the sequence
	 * @param t The strength of the coverage (i.e. the value of parameter <i>t</i>)
	 * @param r A region with additional parameters
	 * @return An instance of the experiment
	 */
	protected SequenceGenerationExperiment<?> getTWayTransitionCoverageExperiment(String algo_name, int t, Region r)
	{
		SequenceGenerationExperiment<MathList<Edge<AtomicEvent>>> e = null;
		if (algo_name.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
		{
			e = new CayleyClosureSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>();			
		}
		else if (algo_name.compareTo(GreedySequenceGenerationExperiment.NAME) == 0)
		{
			e = new GreedyClosureSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>().setRandom(m_random);
			((GreedyClosureSequenceGenerationExperiment<?>) e).setMaxIterations(((MyLaboratory) m_lab).getMaxIterations());
		}
		if (e == null)
		{
			return null;
		}
		e.useClosure(false);
		Automaton a = setAutomaton(e, r);
		if (a == null)
		{
			return null;
		}
		e.setTriagingFunction(new TransitionShallowHistory(a, t), TWAY_TRANSITION);
		e.setStrength(t);
		AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>> factory = new AutomatonCayleyGraphFactory<MathList<Edge<AtomicEvent>>>(a.getAlphabet());
		e.setFactory(factory);
		return e;
	}

	/**
	 * Sets the automaton used in this scenario.
	 * @param e The experiment this automaton will be associated to
	 * @param r A region with optional additional parameters
	 * @return The automaton
	 */
	protected Automaton setAutomaton(SequenceGenerationExperiment<?> e, Region r)
	{
		String spec_name = r.getString(SPECIFICATION);
		if (spec_name == null || !m_specifications.containsKey(spec_name))
		{
			return null;
		}
		String filename = m_specifications.get(spec_name);
		if (!filename.startsWith("/"))
		{
			filename = "/" + filename;
		}
		InputStream is = MyLaboratory.class.getResourceAsStream(filename);
		if (is == null)
		{
			return null;
		}
		Scanner scanner = new Scanner(is);
		Automaton a = null;
		if (filename.endsWith("xml") || filename.endsWith("gff"))
		{
			s_gffParser.reset();
			a = s_gffParser.parse(scanner, "");
		}
		else
		{
			a = Automaton.parseDot(scanner);
		}
		scanner.close();
		a.replaceElse();
		e.setSpecification(a, spec_name);
		return a;
	}
}
