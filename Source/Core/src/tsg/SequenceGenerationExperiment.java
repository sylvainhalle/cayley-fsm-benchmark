/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg;

import ca.uqac.lif.ecp.CayleyGraphFactory;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.Trace;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.ecp.atomic.AutomatonFunction;
import ca.uqac.lif.json.JsonElement;
import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.ExperimentException;

/**
 * An experiment whose task is to generate a set of test sequences from a
 * reactive specification according to various techniques, and to measure
 * experimentally elements such as running time, size of the resulting test
 * suite, etc.
 */
public abstract class SequenceGenerationExperiment<U> extends Experiment
{
	/**
	 * Name of the parameter "Specification"
	 */
	public static final transient String SPECIFICATION = "Specification";
	
	/**
	 * Name of the parameter "Algorithm"
	 */
	public static final transient String ALGORITHM = "Algorithm";

	/**
	 * Name of the parameter "States"
	 */
	public static final transient String STATES = "States";

	/**
	 * Name of the parameter "Transitions"
	 */
	public static final transient String TRANSITIONS = "Transitions";

	/**
	 * Name of the parameter "Function"
	 */
	public static final transient String FUNCTION = "Function";
	
	/**
	 * Name of the parameter "Strength"
	 */
	public static final transient String STRENGTH = "Strength";

	/**
	 * Name of the parameter "Time"
	 */
	public static final transient String TIME = "Time";

	/**
	 * Name of the parameter "Coverage"
	 */
	public static final transient String COVERAGE = "Coverage";
	
	/**
	 * Name of the parameter "Size"
	 */
	public static final transient String SIZE = "Size";
	
	/**
	 * Name of the parameter "Longest"
	 */
	public static final transient String LONGEST = "Longest";
	
	/**
	 * Name of the parameter "Shortest"
	 */
	public static final transient String SHORTEST = "Shortest";
	
	/**
	 * Name of the parameter "Total length"
	 */
	public static final transient String TOTAL_LENGTH = "Total length";
	
	/**
	 * Name of the parameter "Closure"
	 */
	public static final transient String CLOSURE = "Closure";
	
	/**
	 * Value indicating that no closure construction is used
	 */
	public static final transient String CLOSURE_NONE = "None";
	
	/**
	 * Value indicating that the hypergraph construction is used for prefix
	 * closure
	 */
	public static final transient String CLOSURE_HYPERGRAPH = "Hypergraph";
	
	/**
	 * Value indicating that the spanning tree construction is used for prefix
	 * closure
	 */
	public static final transient String CLOSURE_SPANNING = "Spanning tree";

	/**
	 * The automaton used as the reactive specification
	 */
	protected transient Automaton m_specification;
	
	/**
	 * The triaging function
	 */
	protected transient AutomatonFunction<U> m_function;
	
	/**
	 * The factory used to obtain a Cayley graph. Note that all experiments
	 * require a Cayley graph to compute their coverage, even though they
	 * don't use it to generate the test suite.
	 */
	protected transient CayleyGraphFactory<AtomicEvent,U> m_factory;
	
	/**
	 * Whether the experiment uses the prefix closure of the triaging function
	 */
	protected boolean m_useClosure;
	
	/**
	 * The number of times the experiment is repeated
	 */
	protected static final transient int s_numRepetitions = 1;

	/**
	 * Creates a new empty sequence generation experiment.
	 */
	public SequenceGenerationExperiment()
	{
		super();
		setMaxDuration(15000);
		m_useClosure = false;
		setDescription("An experiment whose task is to generate a set of test sequences from a " + 
				"reactive specification according to various techniques, and to measure\n" + 
				"experimentally elements such as running time, size of the resulting test\n" + 
				"suite, etc.");
		describe(SPECIFICATION, "The name of the reactive specification");
		describe(ALGORITHM, "The algorithm used to generate the test suite");
		describe(FUNCTION, "The triaging function used as a basis for coverage");
		describe(CLOSURE, "The type of construction used for the prefix closure, if any");
		describe(STATES, "The number of states in the reactive specification");
		describe(TRANSITIONS, "The number of transitions in the reactive specification");
		describe(TIME, "The time (in milliseconds) taken to generate the test suite");
		describe(COVERAGE, "The coverage reached for this test suite, according to the associated coverage metric");
		describe(SIZE, "Number of sequences in the test suite");
		describe(TOTAL_LENGTH, "Total length of all sequences in the test suite");
		describe(LONGEST, "Length of the longest sequence");
		describe(SHORTEST, "Length of the shortest sequence");
	}

	/**
	 * Sets the specification used as a basis for generating sequences.
	 * @param a The automaton used as the reactive specification
	 * @param name The name given to this specification
	 */
	public void setSpecification(Automaton a, String name)
	{
		m_specification = a;
		setInput(SPECIFICATION, name);
		write(STATES, a.getVertexCount());
		write(TRANSITIONS, a.getEdgeCount());
	}

	/**
	 * Sets the triaging function used as a basis for coverage.
	 * @param f The function
	 * @param name The name given to this function
	 */
	public void setTriagingFunction(AutomatonFunction<U> f, String name)
	{
		m_function = f;
		setInput(FUNCTION, name);
	}
	
	/**
	 * Sets the object used to generate a Cayley graph
	 * @param factory The object
	 */
	public void setFactory(CayleyGraphFactory<AtomicEvent,U> factory)
	{
		m_factory = factory;
	}
	
	/**
	 * Sets whether the experiment uses the prefix closure of the triaging 
	 * function.
	 * @param b Set to <tt>true</tt> to use prefix closure, <tt>false</tt>
	 * otherwise (default)
	 */
	public void useClosure(boolean b)
	{
		m_useClosure = b;
		if (b)
		{
			setInput(CLOSURE, CLOSURE_HYPERGRAPH);
		}
		else
		{
			setInput(CLOSURE, CLOSURE_NONE);
		}
	}
	
	/**
	 * Sets the strength of combinatorial coverage. This is an additional
	 * parameter that makes sense only for experiments whose coverage criterion
	 * is <i>t</i>-way state, action or transition coverage. 
	 * @param t The strength
	 */
	public void setStrength(int t)
	{
		setInput(STRENGTH, t);
		describe(STRENGTH, "The strength of combinatorial coverage (t)");
	}

	@Override
	public int countDataPoints()
	{
		return 4; // size, length, duration, coverage
	}

	@Override
	public void validate()
	{
		float f = readFloatValue(COVERAGE);
		if (f <= 0.5f || f > 1f)
		{
			addWarning("This experiment has a suspicious coverage value of " + f);
		}
		int total_length = readInt(TOTAL_LENGTH);
		if (total_length <= 1)
		{
			addWarning("This experiment generated a suspicious test suite of total length " + total_length);
		}
	}
	
	/**
	 * Computes stats about the generated test suite and writes them into the
	 * experiment's results 
	 * @param suite The generated test suite
	 * @throws ExperimentException If stats reveal the test suite is invalid in
	 * some way
	 */
	protected void writeStats(TestSuite<?> suite) throws ExperimentException
	{
		int total_size = 0, total_length = 0, total_shortest = 0, total_longest = 0;
		if (suite == null)
		{
			addWarning("The algorithm failed to generate a test suite");
			return;
		}
		total_size += suite.size();
		total_length += suite.getTotalLength();
		int len_max = -1;
		int len_min = -1;
		for (Trace<?> t : suite)
		{
			int len = t.size();
			if (len_max == -1 || len > len_max)
			{
				len_max = len;
			}
			if (len_min == -1 || len < len_min)
			{
				len_min = len;
			}			
		}
		total_shortest += len_min;
		total_longest += len_max;
		write(SIZE, (float) total_size / (float) s_numRepetitions);
		write(TOTAL_LENGTH, (float) total_length / (float) s_numRepetitions);
		write(SHORTEST, (float) total_shortest / (float) s_numRepetitions);
		write(LONGEST, (float) total_longest / (float) s_numRepetitions);
		System.out.println(readString(SPECIFICATION) + "," + readString(ALGORITHM) + "," + suite);
	}
	
	/**
	 * Reads a float value from an experiment's parameters. This method is there
	 * to circumvent a bug in LabPal with its {@link #readFloat(String)} method
	 * (which actually returns an <tt>int</tt>).
	 * @param parameter The name of the parameter
	 * @return The float value, or 0 if the parameter is not numeric
	 */
	public float readFloatValue(String parameter)
	{
		float v = 0;
		JsonElement je = read(parameter);
		if (je instanceof JsonNumber)
		{
			v = ((JsonNumber) je).numberValue().floatValue();
		}
		return v;
	}
}
