/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.graphwalker;

import org.graphwalker.core.machine.Context;
import org.graphwalker.core.machine.SimpleMachine;

import static org.graphwalker.core.common.Objects.isNotNull;

/**
 * A descendant of GraphWalker's {@link SimpleMachine} that overrides some of
 * its methods to avoid writing log entries to the console. These methods are
 * a direct copy-paste of the originals, where the lines that write to the
 * log have been removed.
 * @author Sylvain Hallé
 */
public class SilentSimpleMachine extends SimpleMachine
{
	public SilentSimpleMachine(Context c)
	{
		super(c);
	}
	
	/*
	private Context chooseSharedContext(Context context, RuntimeVertex vertex) {
    List<SharedStateTuple> candidates = getPossibleSharedStates(vertex.getSharedState());
    SharedStateTuple candidate = candidates.get(SingletonRandomGenerator.nextInt(candidates.size()));
    if (!candidate.getVertex().equals(context.getCurrentElement())) {
      candidate.context.setNextElement(candidate.getVertex());
      context = switchContext(candidate.context);
    } else {
      lastElement = null;
    }
    return context;
  }
  */

	@Override
	protected Context getNextStep(Context context)
	{
    if (isNotNull(context.getNextElement())) {
      context.setCurrentElement(context.getNextElement());
    } else {
      context.getPathGenerator().getNextStep();
    }
    return context;
  }
}
