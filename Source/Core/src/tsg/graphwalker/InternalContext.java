/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.graphwalker;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;

import javax.script.Bindings;
import javax.script.SimpleBindings;

import org.graphwalker.core.generator.PathGenerator;
import org.graphwalker.core.machine.ExecutionContext;
import org.graphwalker.core.machine.ExecutionStatus;
import org.graphwalker.core.machine.MachineException;
import org.graphwalker.core.model.Edge;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.model.Model;
import org.graphwalker.core.model.Vertex;

import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;

public class InternalContext extends ExecutionContext 
{
	@SuppressWarnings("unused")
	private static final Bindings bindings = new SimpleBindings();
	
	/**
	 * A reference to the initial vertex of the GraphWalker specification 
	 */
	protected Vertex m_initialVertex;
	
	/**
	 * A copy of the field of same name in {@link ExecutionContext}. It is
	 * duplicated here because it is private in the parent, and the
	 * method {@link #execute(Element)}, which we override in this class,
	 * modifies its value.
	 */
	private ExecutionStatus executionStatus = ExecutionStatus.NOT_EXECUTED;

	/**
	 * Creates a new empty instance of the context.
	 */
	public InternalContext()
	{
		super();
	}

	public InternalContext(Automaton specification, PathGenerator<?> generator)
	{
		super();
		setModel(getModelFromAutomaton(specification).build());
		setPathGenerator(generator);
		setCurrentElement(m_initialVertex.build());
	}

	public InternalContext(Model model, PathGenerator<?> generator) 
	{
		super(model, generator);
	}

	public InternalContext(Model.RuntimeModel model, PathGenerator<?> generator) 
	{
		super(model, generator);
	}
	
	/**
	 * Resets the execution context by setting its current element to the initial
	 * state of the specification. This method is used by the
	 * {@link GraphWalkerSequenceGenerationExperiment} to call the sequence
	 * generator multiple times in order to get different sequences.
	 */
	public void reset()
	{
		setCurrentElement(m_initialVertex.build());
	}
	
	/**
	 * A direct copy of parent's {@link ExecutionContext#execute(Element)}, with
	 * the lines that write entries to the log being removed. GraphWalker
	 * writes log entries profusely (two lines every time a transition is taken
	 * in an automaton), which clutters the console and also slows down the
	 * execution of the algorithm.
	 */
	@Override
	public void execute(Element element) {
    if (!element.hasName()) {
      return;
    }
    try {
      Method method = getClass().getMethod(element.getName());
      method.invoke(this);
    } catch (NoSuchMethodException e) {
      // ignore, method is not defined in the execution context
    } catch (Throwable t) {
      executionStatus = ExecutionStatus.FAILED;
      throw new MachineException(this, t);
    }
  }
	
	@Override
	public ExecutionStatus getExecutionStatus() {
    return executionStatus;
  }

	@Override
  public InternalContext setExecutionStatus(ExecutionStatus executionStatus) {
    this.executionStatus = executionStatus;
    return this;
  }

	/**
	 * Converts a SealTest {@link Automaton} into a GraphWalker {@link Model}.
	 * The method also stores in {@link #m_initialVertex} a reference to the
	 * initial state of the GraphWalker model. 
	 * @param specification The original automaton
	 * @return The GraphWalker model
	 */
	protected Model getModelFromAutomaton(Automaton specification)
	{
		Model m = new Model();
		Map<Integer,Vertex> m_vertices = new HashMap<Integer,Vertex>();
		for (ca.uqac.lif.ecp.graphs.Vertex<AtomicEvent> v : specification.getVertices())
		{
			Vertex n_v = new Vertex();
			n_v.setName(Integer.toString(v.getId()));
			m_vertices.put(v.getId(), n_v);
			m.addVertex(n_v); 
		}
		for (ca.uqac.lif.ecp.Edge<AtomicEvent> e : specification.getEdges())
		{
			Edge n_e = new Edge();
			n_e.setSourceVertex(m_vertices.get(e.getSource()));
			n_e.setTargetVertex(m_vertices.get(e.getDestination()));
			n_e.setName(e.getLabel().getLabel());
			m.addEdge(n_e);
		}
		m_initialVertex = m_vertices.get(specification.getInitialVertex().getId());
		return m;
	}
}
