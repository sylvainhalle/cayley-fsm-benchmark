/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.graphwalker;

import java.util.HashSet;
import java.util.Set;

import org.graphwalker.core.generator.PathGenerator;
import org.graphwalker.core.generator.SingletonRandomGenerator;
import org.graphwalker.core.model.Edge.RuntimeEdge;
import org.graphwalker.core.model.Element;
import org.graphwalker.core.model.Vertex.RuntimeVertex;
import org.graphwalker.dsl.antlr.generator.GeneratorFactory;

import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.labpal.ExperimentException;
import ca.uqac.lif.labpal.Random;
import tsg.SequenceGenerationExperiment;

/**
 * Generates sequences based on an automaton specification, using the
 * <a href="https://graphwalker.org">GraphWalker</a> sequence generator.
 * GraphWalker is tested with state coverage and transition coverage. The
 * other metrics found in this benchmark (state/transition residual, t-way,
 * etc.) are not supported by GraphWalker.
 * @author Sylvain Hallé
 *
 * @param <U> The type of the categories for the associated coverage metric
 */
public class GraphWalkerSequenceGenerationExperiment<U> extends SequenceGenerationExperiment<U>
{
	/**
	 * Name of the algorithm
	 */
	public static final transient String NAME = "GraphWalker";

	/**
	 * The maximum number of iterations
	 */
	protected transient int m_maxIterations = 1;

	/**
	 * A random source
	 */
	protected transient Random m_random;

	/**
	 * A set of visited vertices, used to measure state coverage
	 */
	protected Set<Integer> m_vertices;

	/**
	 * A set of visited edges, used to measure transition coverage
	 */
	protected Set<ca.uqac.lif.ecp.Edge<AtomicEvent>> m_edges;

	/**
	 * A string used to represent the coverage metric to achieve by the generator
	 */
	protected transient String m_generatorString;
	
	/**
	 * A Boolean flag that determines what coverage metric (state or transition)
	 * is being calculated in this experiment.
	 */
	protected boolean m_isStateCoverage;

	/**
	 * Creates a new instance of the experiment.
	 */
	public GraphWalkerSequenceGenerationExperiment()
	{
		super();
		setInput(ALGORITHM, NAME);
		m_vertices = new HashSet<Integer>();
		m_edges = new HashSet<ca.uqac.lif.ecp.Edge<AtomicEvent>>();
	}

	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		PathGenerator<?> p_generator = GeneratorFactory.parse(m_generatorString);
		InternalContext context = new InternalContext(m_specification, p_generator);
		boolean coverage_achieved = false;
		int num_seqs = 0;
		long start_time = System.currentTimeMillis();
		SilentSimpleMachine machine = new SilentSimpleMachine(context);
		float best_coverage = 0;
		for (int i = 0; i < 10 && !coverage_achieved; i++)
		{
			int max_steps = pickLength();
			int len = 0;
			SingletonRandomGenerator.setSeed(m_random.nextInt());
			int steps = 0;
			while (machine.hasNextStep() && ++steps < max_steps && !coverage_achieved) 
			{
				machine.getNextStep();
				Element e = context.getCurrentElement();
				if (m_isStateCoverage && e instanceof RuntimeVertex)
				{
					len++;
				}
				else if (!m_isStateCoverage && e instanceof RuntimeEdge)
				{
					len++;
				}
				coverage_achieved = coverageAchieved(e);
			}
			float coverage = getCoverage();
			if (coverage > best_coverage)
			{
				write(TOTAL_LENGTH, readInt(TOTAL_LENGTH) + len);
				num_seqs++;
				best_coverage = coverage;
			}
			context.reset();
			machine = new SilentSimpleMachine(context);
		}
		long end_time = System.currentTimeMillis();
		write(COVERAGE, best_coverage);
		write(TIME, end_time - start_time);
		write(SIZE, num_seqs);
	}

	@Override
	public void setSpecification(Automaton a, String name)
	{
		super.setSpecification(a, name);
		m_vertices.addAll(a.getVertexLabels());
		m_vertices.remove(a.getInitialVertex().getId()); // Since initial state is always visited
		m_edges.addAll(a.getEdges());
	}

	protected float getCoverage()
	{
		if (m_generatorString.contains("vertex"))
		{
			// State coverage
			float states = readInt(STATES);
			float size = (float) m_vertices.size();
			return (states - size) / states;
		}
		else
		{
			// Transition coverage
			float edges = readInt(TRANSITIONS);
			float size = (float) m_edges.size();
			return (edges - size) / edges;
		}
	}

	protected boolean coverageAchieved(Element e)
	{
		if (m_isStateCoverage)
		{
			// State coverage
			if (!(e instanceof RuntimeVertex))
			{
				return false;
			}
			int id = Integer.parseInt(((RuntimeVertex) e).getName());
			m_vertices.remove(id);
			if (m_vertices.isEmpty())
			{
				return true;
			}
		}
		else
		{
			// Transition coverage
			if (!(e instanceof RuntimeEdge))
			{
				return false;
			}
			RuntimeEdge ed = (RuntimeEdge) e;
			int source_id = Integer.parseInt(ed.getSourceVertex().getName());
			int dest_id = Integer.parseInt(ed.getTargetVertex().getName());
			ca.uqac.lif.ecp.Edge<AtomicEvent> s_e = new ca.uqac.lif.ecp.Edge<AtomicEvent>(source_id, new AtomicEvent(ed.getName()), dest_id);
			m_edges.remove(s_e);
			if (m_edges.isEmpty())
			{
				return true;
			}
		}
		return false;
	}

	public void setGeneratorString(String s)
	{
		m_generatorString = s;
		if (s.contains("vertex"))
		{
			m_isStateCoverage = true;
		}
		else
		{
			m_isStateCoverage = false;
		}
	}

	/**
	 * Sets the maximum number of iterations to retry
	 * @param max_iterations The number of iterations
	 */
	public void setMaxIterations(int max_iterations)
	{
		m_maxIterations = max_iterations;
	}
	
	protected int pickLength()
	{
		return m_random.nextInt(readInt(TRANSITIONS) * 2);
	}

	/**
	 * Sets the random source used by the generator.
	 * @param r The random source
	 * @return This experiment
	 */
	public GraphWalkerSequenceGenerationExperiment<U> setRandom(Random r)
	{
		m_random = r;
		return this;
	}
}
