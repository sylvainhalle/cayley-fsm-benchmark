This folder contains the finite-state machine specifications used in the
benchmark. Please refer to the Readme file within each sub-folder for an
additional description of their origin.