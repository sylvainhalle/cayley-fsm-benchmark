All the FSM in this folder have been extracted from this repository
of reactive specification patterns:
https://patterns.projects.cs.ksu.edu/documentation/patterns/ltl.shtml

The name of the file is the LTL formula equivalent to the automaton
contained in the file.