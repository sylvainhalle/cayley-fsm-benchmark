/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import static tsg.SequenceGenerationExperiment.COVERAGE;

import ca.uqac.lif.json.JsonElement;
import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.LatexNamer;
import ca.uqac.lif.labpal.provenance.ExperimentValue;
import ca.uqac.lif.petitpoucet.NodeFunction;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;

/**
 * Computes the minimum coverage (in percentage) achieved by an algorithm on
 * the set of problem instances.
 */
public class MinCoverageMacro extends ExperimentMacro
{
	/**
	 * The name of the algorithm
	 */
	protected String m_algorithm;

	/**
	 * The experiment containing the minimum coverage found
	 */
	protected SequenceGenerationExperiment<?> m_minExperiment;

	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab to which the macro is attached
	 * @param algorithm The name of the algorithm used to generate the
	 * sequences
	 */
	public MinCoverageMacro(MyLaboratory lab, String algorithm)
	{
		super(lab, "minCoverage" + LatexNamer.latexify(algorithm));
		setDescription("The minimum coverage (in percentage) achieved by the algorithm " + algorithm + " on the set of problem instances.");
		m_algorithm = algorithm;
		m_minExperiment = null;
	}

	@Override
	public JsonElement getValue()
	{
		float min = 1;
		for (SequenceGenerationExperiment<?> e : m_experiments)
		{
			Experiment.Status status = e.getStatus();
			if (status != Experiment.Status.DONE && status != Experiment.Status.DONE_WARNING)
			{
				continue;
			}
			float coverage = e.readFloatValue(COVERAGE);
			if (coverage < min)
			{
				min = coverage;
				m_minExperiment = e;
			}
		}
		return new JsonNumber((int) (min * 100));
	}

	@Override
	public NodeFunction getDependency()
	{
		if (m_minExperiment == null)
		{
			return null;
		}
		return new ExperimentValue(m_minExperiment, COVERAGE);
	}
}
