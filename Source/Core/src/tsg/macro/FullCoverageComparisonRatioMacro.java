/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.NumberHelper;
import ca.uqac.lif.mtnp.table.TempTable;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;
import tsg.table.ComparisonTable;

/**
 * Computes the weighted average size ratio between the test suites produced by 
 * greedy algorithm and the Cayley algorithm over all problem instances.
 */
public class FullCoverageComparisonRatioMacro extends WeightedComparisonRatioMacro
{
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab this macro is attached to
	 * @param table The x-y table that puts on each line a metric computed
	 * for each test suite for both algorithms
	 * @param algorithm The name of the algorithm the Cayley approach is
	 * compared to
	 * @param element The parameter that is being extracted from each
	 * experiment
	 */
	public FullCoverageComparisonRatioMacro(MyLaboratory lab, ComparisonTable table, String algorithm, String element)
	{
		super(lab, table, algorithm, element + "WC", SequenceGenerationExperiment.COVERAGE);
		setDescription("The average " + element + " between the test suites with full coverage produced by " + algorithm + " and the Cayley algorithm over all problem instances");
	}
	
	@Override
	public JsonNumber getValue()
	{
		float sum = 0, total = 0;
		TempTable dt = m_table.getDataTable();
		for (int i = 0; i < dt.getRowCount(); i++)
		{
			float x = dt.get(0, i).numberValue().floatValue(); // row/col is inverted in Hard
			float y = dt.get(1, i).numberValue().floatValue();
			float w_x = getWeight(dt.getDependency(i, 0));
			float w_y = getWeight(dt.getDependency(i, 1));
			if (x == 0 || y == 0 || w_x < 1 || w_y < 1)
			{
				// Null values indicate the experiments haven't run yet; ignore
				continue;
			}
			sum += y / x;
			total++;
		}
		if (sum == 0)
		{
			return new JsonNumber(0);
		}
		return new JsonNumber(NumberHelper.roundToSignificantFigures(sum / total, 3));
	}
}
