/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import static tsg.cayleygraph.CayleySequenceGenerationExperiment.CAYLEY_STATES;

import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.provenance.ExperimentValue;
import ca.uqac.lif.petitpoucet.NodeFunction;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;

/**
 * Macro that computes statistics on the size of Cayley graphs.
 */
public abstract class CayleySizeMacro extends ExperimentMacro
{
	/**
	 * The experiment that contains the extremal value computed by the macro
	 */
	protected SequenceGenerationExperiment<?> m_extremum;

	/**
	 * Creates a new macro.
	 * @param lab The lab this macro is attached to
	 * @param name The name of the macro
	 */
	public CayleySizeMacro(MyLaboratory lab, String name)
	{
		super(lab, name);
		m_extremum = null;
	}
	
	@Override
	public NodeFunction getDependency()
	{
		return new ExperimentValue(m_extremum, CAYLEY_STATES);
	}

	/**
	 * Calculates the minimum size of all Cayley graphs generated.
	 */
	public static class CayleyMinSizeMacro extends CayleySizeMacro
	{
		/**
		 * Creates a new instance of the macro
		 * @param lab The lab this macro is attached to
		 */
		public CayleyMinSizeMacro(MyLaboratory lab)
		{
			super(lab, "minCayleySize");
			setDescription("The minimum size of all Cayley graphs generated");
		}

		@Override
		public JsonNumber getValue()
		{
			int min = -1;
			for (SequenceGenerationExperiment<?> e : m_experiments)
			{
				int size = e.readInt(CAYLEY_STATES);
				if (size > 0)
				{
					// A size of 0 means the experiment has not run yet
					if (min < 0 || min > size)
					{
						min = size;
						m_extremum = e;
					}
				}
			}
			return new JsonNumber(min);
		}
	}
	
	/**
	 * Calculates the maximum size of all Cayley graphs generated.
	 */
	public static class CayleyMaxSizeMacro extends CayleySizeMacro
	{
		/**
		 * Creates a new instance of the macro
		 * @param lab The lab this macro is attached to
		 */
		public CayleyMaxSizeMacro(MyLaboratory lab)
		{
			super(lab, "maxCayleySize");
			setDescription("The maximum size of all Cayley graphs generated");
		}

		@Override
		public JsonNumber getValue()
		{
			int max = -1;
			for (SequenceGenerationExperiment<?> e : m_experiments)
			{
				int size = e.readInt(CAYLEY_STATES);
				if (size > 0)
				{
					// A size of 0 means the experiment has not run yet
					if (size > max)
					{
						max = size;
						m_extremum = e;
					}
				}
			}
			return new JsonNumber(max);
		}
	}
}
