/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import static tsg.SequenceGenerationExperiment.STRENGTH;

import ca.uqac.lif.labpal.LatexNamer;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;

/**
 * Computes the average coverage (in percentage) achieved by an algorithm on
 * the set of combinatorial problem instances for a single value of
 * coverage strength (i.e. the parameter <i>t</i>).
 */
public class TCoverageMacro extends AverageCoverageMacro
{
	/**
	 * The coverage strength
	 */
	protected int m_strength;
	
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab to which the macro is attached
	 * @param algorithm The name of the algorithm used to generate the
	 * sequences
	 * @param t The coverage strength
	 */
	public TCoverageMacro(MyLaboratory lab, String algorithm, int t)
	{
		super(lab, "avgCoverage" + LatexNamer.latexify(algorithm + t));
		setDescription("The average coverage (in percentage) achieved by the algorithm " + algorithm + " on the set of t-way instances for t = " + t);
		m_algorithm = algorithm;
		m_strength = t;
	}
	
	@Override
	protected boolean appliesTo(SequenceGenerationExperiment<?> e)
	{
		return e.readInt(STRENGTH) == m_strength;
	}
}
