/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.NumberHelper;
import ca.uqac.lif.labpal.provenance.ExperimentValue;
import ca.uqac.lif.mtnp.table.TempTable;
import ca.uqac.lif.petitpoucet.NodeFunction;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;
import tsg.table.ComparisonTable;

/**
 * Computes the weighted average size ratio between the test suites produced by 
 * an algorithm and the Cayley algorithm over all problem instances.
 */
public class WeightedComparisonRatioMacro extends ComparisonRatioMacro
{
	protected transient String m_weightParameter;
	
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab this macro is attached to
	 * @param table The x-y table that puts on each line a metric computed
	 * for each test suite for both algorithms
	 * @param algorithm The name of the algorithm the Cayley approach is
	 * compared to
	 * @param element The parameter that is being extracted from each
	 * experiment
	 * @param weight_parameter The parameter being used as the weight
	 * in each experiment.
	 */
	public WeightedComparisonRatioMacro(MyLaboratory lab, ComparisonTable table, String algorithm, String element, String weight_parameter)
	{
		super(lab, table, algorithm, element + "W");
		m_weightParameter = weight_parameter;
		setDescription("The weighted average " + element + " between the test suites produced by " + algorithm + " and the Cayley algorithm over all problem instances");
	}
	
	@Override
	public JsonNumber getValue()
	{
		float sum = 0, total = 0;
		TempTable dt = m_table.getDataTable();
		for (int i = 0; i < dt.getRowCount(); i++)
		{
			float x = dt.get(i, 0).numberValue().floatValue();
			float y = dt.get(i, 1).numberValue().floatValue();
			float w_x = getWeight(dt.getDependency(i, 0));
			float w_y = getWeight(dt.getDependency(i, 1));
			if (x == 0 || y == 0 || w_x == 0 || w_y == 0)
			{
				// Null values indicate the experiments haven't run yet; ignore
				continue;
			}
			sum += (y * w_x) / (x * w_y);
			total++;
		}
		if (sum == 0)
		{
			return new JsonNumber(0);
		}
		return new JsonNumber(NumberHelper.roundToSignificantFigures(sum / total, 3));
	}
	
	protected float getWeight(NodeFunction nf)
	{
		if (!(nf instanceof ExperimentValue))
		{
			return 0;
		}
		ExperimentValue ev = (ExperimentValue) nf;
		return ((SequenceGenerationExperiment<?>) ev.getOwner()).readFloatValue(m_weightParameter);
	}
}
