/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.LatexNamer;
import ca.uqac.lif.labpal.Experiment.Status;
import ca.uqac.lif.labpal.provenance.ExperimentValue;
import ca.uqac.lif.petitpoucet.NodeFunction;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;

/**
 * Computes the maximum value of an experiment parameter, for experiments
 * pertaining to a single algorithm.
 */
public class MaxMacro extends ExperimentMacro
{
	/**
	 * The node function linking to the experiment with the maximum number
	 * of sequences in the test suite
	 */
	protected NodeFunction m_dependency;
	
	/**
	 * The experiment parameter whose maximum value is to be found
	 */
	protected String m_parameter;
	
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab to which the macro is attached
	 * @param algo_name The generation algorithm that is common to all the
	 * experiments given to this macro
	 * @param element A textual description of the parameter over which the
	 * calculation is being made
	 * @param param_name The name of the experiment parameter to fetch
	 */
	public MaxMacro(MyLaboratory lab, String algo_name, String element, String param_name)
	{
		super(lab, LatexNamer.latexify("max" + param_name + algo_name));
		setDescription("The maximum " + element + " in experiments of algorithm " + algo_name);
		m_parameter = param_name;
	}
	
	@Override
	public JsonNumber getValue()
	{
		int m_maxSize = 0;
		for (SequenceGenerationExperiment<?> e : m_experiments)
		{
			if (e.getStatus() != Status.DONE)
			{
				continue;
			}
			int size = e.readInt(m_parameter);
			if (size > m_maxSize)
			{
				m_maxSize = size;
				m_dependency = new ExperimentValue(e, m_parameter);
			}
		}
		return new JsonNumber(m_maxSize);
	}
}
