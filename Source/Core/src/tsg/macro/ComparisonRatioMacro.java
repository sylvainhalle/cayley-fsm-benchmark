/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.LatexNamer;
import ca.uqac.lif.labpal.NumberHelper;
import ca.uqac.lif.labpal.macro.MacroScalar;
import ca.uqac.lif.mtnp.table.TableEntry;
import tsg.MyLaboratory;
import tsg.table.ComparisonTable;

/**
 * Computes he average size ratio between the test suites produced by 
 * greedy algorithm and the Cayley algorithm over all problem instances.
 */
public class ComparisonRatioMacro extends MacroScalar
{
	/**
	 * The x-y table that puts on each line the size of each test suite for
	 * both algorithms
	 */
	protected ComparisonTable m_table;
	
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab this macro is attached to
	 * @param table The x-y table that puts on each line a metric computed
	 * for each test suite for both algorithms
	 * @param algorithm The name of the algorithm the Cayley approach is
	 * compared to
	 * @param element The parameter that is being extracted from each
	 * experiment
	 */
	public ComparisonRatioMacro(MyLaboratory lab, ComparisonTable table, String algorithm, String element)
	{
		super(lab, LatexNamer.latexify("tComparison" + element + algorithm));
		m_table = table;
		setDescription("The average " + element + " between the test suites produced by " + algorithm + " and the Cayley algorithm over all problem instances");
	}
	
	@Override
	public JsonNumber getValue()
	{
		float sum = 0, total = 0;
		String caption_x = m_table.getCaptionX();
		String caption_y = m_table.getCaptionY();
		for (TableEntry te : m_table.getDataTable().getEntries())
		{
			float x = te.get(caption_x).numberValue().floatValue();
			float y = te.get(caption_y).numberValue().floatValue();
			if (x == 0 || y == 0)
			{
				// Null values indicate the experiments haven't run yet; ignore
				continue;
			}
			sum += y / x;
			total++;
		}
		if (sum == 0)
		{
			return new JsonNumber(0);
		}
		return new JsonNumber(NumberHelper.roundToSignificantFigures(sum / total, 3));
	}
}
