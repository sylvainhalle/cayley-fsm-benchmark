/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import static tsg.SequenceGenerationExperiment.COVERAGE;

import ca.uqac.lif.json.JsonElement;
import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.LatexNamer;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;

/**
 * Computes the average coverage (in percentage) achieved by an algorithm on
 * the set of problem instances.
 */
public class AverageCoverageMacro extends ExperimentMacro
{
	/**
	 * The name of the algorithm
	 */
	protected String m_algorithm;
	
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab to which the macro is attached
	 * @param algorithm The name of the algorithm used to generate the
	 * sequences
	 */
	public AverageCoverageMacro(MyLaboratory lab, String algorithm)
	{
		super(lab, "avgCoverage" + LatexNamer.latexify(algorithm));
		setDescription("The average coverage (in percentage) achieved by the algorithm " + algorithm + " on the set of problem instances.");
		m_algorithm = algorithm;
	}
	
	@Override
	public JsonElement getValue()
	{
		float total = 0;
		float size = 0;
		for (SequenceGenerationExperiment<?> e : m_experiments)
		{
			Experiment.Status status = e.getStatus();
			if (status != Experiment.Status.DONE || !appliesTo(e))
			{
				continue;
			}
			float coverage = e.readFloatValue(COVERAGE);
			if (coverage > 1)
			{
				System.err.println("Coverage of " + coverage);
			}
			total += coverage;
			size++;
		}
		if (size == 0)
		{
			return new JsonNumber(0);
		}
		float avg = total / size;
		if (avg > 1)
		{
			System.err.println("Average of " + avg);
		}
		return new JsonNumber((int) (avg * 100));
	}
	
	/**
	 * Determines whether an experiment is part of those to be used in the macro's
	 * computation.
	 * @param e The experiment
	 * @return <tt>true</tt> if the experiment is to be included, <tt>false</tt>
	 * otherwise
	 */
	protected boolean appliesTo(SequenceGenerationExperiment<?> e)
	{
		return true;
	}
}
