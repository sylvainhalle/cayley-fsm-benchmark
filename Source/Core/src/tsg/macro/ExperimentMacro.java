/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.labpal.macro.MacroScalar;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;

/**
 * Computes a scalar value from a set of experiments.
 */
public abstract class ExperimentMacro extends MacroScalar
{
	/**
	 * The set of experiments
	 */
	protected Set<SequenceGenerationExperiment<?>> m_experiments;

	/**
	 * Creates a new experiment macro
	 * @param lab The lab to which the macro is attached
	 * @param name The name of the macro
	 */
	public ExperimentMacro(MyLaboratory lab, String name)
	{
		super(lab, name);
		m_experiments = new HashSet<SequenceGenerationExperiment<?>>();
	}
	
	/**
	 * Adds an experiment to the macro.
	 * @param e The experiment
	 * @return This macro
	 */
	public ExperimentMacro add(SequenceGenerationExperiment<?> e)
	{
		if (e != null)
		{
			m_experiments.add(e);
		}
		return this;
	}
}
