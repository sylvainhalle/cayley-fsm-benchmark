/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.macro;

import static tsg.SequenceGenerationExperiment.COVERAGE;

import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.Experiment.Status;
import tsg.MyLaboratory;
import tsg.SequenceGenerationExperiment;
import ca.uqac.lif.labpal.LatexNamer;
import ca.uqac.lif.labpal.NumberHelper;

/**
 * Computes the percentage of problem instances for which an algorithm
 * fails to achieve complete coverage.
 */
public class IncompleteCoverageCount extends ExperimentMacro
{
	/**
	 * Creates a new instance of the macro.
	 * @param lab The lab to which the macro is attached
	 * @param algo_name The generation algorithm that is common to all the
	 * experiments given to this macro
	 */
	public IncompleteCoverageCount(MyLaboratory lab, String algo_name)
	{
		super(lab, LatexNamer.latexify("incompleteCoverage" + algo_name));
		setDescription("The percentage of problem instances for which the algorithm " + algo_name + " fails to achieve complete coverage");
	}
	
	@Override
	public JsonNumber getValue()
	{
		float num_incomplete = 0, total = 0;
		for (SequenceGenerationExperiment<?> e : m_experiments)
		{
			total++;
			if (e.getStatus() != Status.DONE || e.readFloatValue(COVERAGE) < 1)
			{
				num_incomplete++;
			}
		}
		if (total == 0)
		{
			return new JsonNumber(0);
		}
		return new JsonNumber(NumberHelper.roundToSignificantFigures((num_incomplete * 100) / total, 2));
	}
}
