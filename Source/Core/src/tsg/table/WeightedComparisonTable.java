/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.table;

import ca.uqac.lif.json.JsonNull;
import ca.uqac.lif.json.JsonNumber;
import ca.uqac.lif.labpal.provenance.ExperimentValue;
import ca.uqac.lif.mtnp.table.TableEntry;
import ca.uqac.lif.mtnp.table.TempTable;
import tsg.SequenceGenerationExperiment;

/**
 * Table creating (x,y) points from the results of pairs of experiments.
 * @author Sylvain Hallé
 */
public class WeightedComparisonTable extends ComparisonTable
{
	/**
	 * The name of the parameter used to weigh each result
	 */
	protected final String m_weightParameter;

	/**
	 * Creates a new empty table
	 * @param parameter The name of the parameter to read in each experiment
	 * @param weight_parameter The name of the parameter used to weigh
	 * each result
	 */
	public WeightedComparisonTable(String parameter, String weight_parameter)
	{
		super(parameter);
		m_weightParameter = weight_parameter;
	}

	/**
	 * Creates a new empty table.
	 * @param parameter The name of the parameter to read in each experiment
	 * @param weight_parameter The name of the parameter used to weigh
	 * each result
	 * @param caption_x The name of the first column
	 * @param caption_y The name of the second column
	 */
	public WeightedComparisonTable(String parameter, String weight_parameter, String caption_x, String caption_y)
	{
		super(parameter, caption_x, caption_y);
		m_weightParameter = weight_parameter;
	}
	
	@Override
	public TempTable getDataTable(boolean temporary)
	{
		TempTable table = new TempTable(getId(), m_captionX, m_captionY);
		table.setId(getId());
		for (ExperimentPair pair : m_pairs)
		{
			Object x = pair.getExperimentX().read(m_parameter);
			Object y = pair.getExperimentY().read(m_parameter);
			float w_x = ((SequenceGenerationExperiment<?>) pair.getExperimentX()).readFloatValue(m_weightParameter);
			float w_y = ((SequenceGenerationExperiment<?>) pair.getExperimentY()).readFloatValue(m_weightParameter);
			if (x == null || y == null || x instanceof JsonNull || y instanceof JsonNull || w_x == 0 || w_y == 0 || discard(pair.getExperimentX()) || discard(pair.getExperimentY()))
			{
				continue;
			}
			TableEntry te = new TableEntry();
			if (x instanceof JsonNumber)
			{
				te.put(m_captionX, ((JsonNumber) x).numberValue().floatValue() * w_y);
			}
			else
			{
				te.put(m_captionX, x);
			}
			if (y instanceof JsonNumber)
			{
				te.put(m_captionY, ((JsonNumber) y).numberValue().floatValue() * w_x);
			}
			else
			{
				te.put(m_captionY, y);
			}
			te.addDependency(m_captionX, new ExperimentValue(pair.getExperimentX(), m_parameter));
			te.addDependency(m_captionX, new ExperimentValue(pair.getExperimentY(), m_weightParameter));
			te.addDependency(m_captionY, new ExperimentValue(pair.getExperimentY(), m_parameter));
			te.addDependency(m_captionY, new ExperimentValue(pair.getExperimentX(), m_weightParameter));
			table.add(te);
		}
		return table;
	}
}
