/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg;

import static tsg.SequenceGenerationExperiment.ALGORITHM;
import static tsg.SequenceGenerationExperiment.CLOSURE;
import static tsg.SequenceGenerationExperiment.CLOSURE_NONE;
import static tsg.SequenceGenerationExperiment.CLOSURE_SPANNING;
import static tsg.SequenceGenerationExperiment.COVERAGE;
import static tsg.SequenceGenerationExperiment.FUNCTION;
import static tsg.SequenceGenerationExperiment.SIZE;
import static tsg.SequenceGenerationExperiment.SPECIFICATION;
import static tsg.SequenceGenerationExperiment.STATES;
import static tsg.SequenceGenerationExperiment.STRENGTH;
import static tsg.SequenceGenerationExperiment.TIME;
import static tsg.SequenceGenerationExperiment.TOTAL_LENGTH;
import static tsg.SequenceGenerationExperiment.TRANSITIONS;
import static tsg.SequenceGenerationExperimentFactory.ACTION_COVERAGE;
import static tsg.SequenceGenerationExperimentFactory.ACTION_RESIDUAL;
import static tsg.SequenceGenerationExperimentFactory.STATE_COVERAGE;
import static tsg.SequenceGenerationExperimentFactory.STATE_RESIDUAL;
import static tsg.SequenceGenerationExperimentFactory.TRANSITION_COVERAGE;
import static tsg.SequenceGenerationExperimentFactory.TRANSITION_RESIDUAL;
import static tsg.SequenceGenerationExperimentFactory.TWAY_ACTION;
import static tsg.SequenceGenerationExperimentFactory.TWAY_STATE;
import static tsg.SequenceGenerationExperimentFactory.TWAY_TRANSITION;

import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import ca.uqac.lif.labpal.Experiment;
import ca.uqac.lif.labpal.FileHelper;
import ca.uqac.lif.labpal.Laboratory;
import ca.uqac.lif.labpal.LatexNamer;
import ca.uqac.lif.labpal.Region;
import ca.uqac.lif.labpal.table.ExperimentTable;
import ca.uqac.lif.mtnp.plot.TwoDimensionalPlot.Axis;
import ca.uqac.lif.mtnp.plot.gnuplot.Scatterplot;
import tsg.cayleygraph.CayleySequenceGenerationExperiment;
import tsg.direct.DirectSequenceGenerationExperiment;
import tsg.graphwalker.GraphWalkerSequenceGenerationExperiment;
import tsg.greedy.GreedySequenceGenerationExperiment;
import tsg.macro.AverageCoverageMacro;
import tsg.macro.CayleySizeMacro;
import tsg.macro.ComparisonRatioMacro;
import tsg.macro.FullCoverageComparisonRatioMacro;
import tsg.macro.IncompleteCoverageCount;
import tsg.macro.LabStats;
import tsg.macro.MaxMacro;
import tsg.macro.MinCoverageMacro;
import tsg.macro.TCoverageMacro;
import tsg.table.ComparisonTable;
import tsg.table.WeightedComparisonTable;

public class MyLaboratory extends Laboratory
{
	/**
	 * A regex pattern used to fetch the name inside a specification file
	 */
	protected static final transient Pattern s_namePattern = Pattern.compile("Name: (.*)");

	/**
	 * A factory to generate experiment instances
	 */
	protected transient SequenceGenerationExperimentFactory m_factory;

	/**
	 * The maximum value of <i>t</i> used in <i>t</i>-way sequence generation
	 * instances
	 */
	protected int m_maxT = 3;

	/**
	 * The maximum number of iterations authorized for the greedy algorithm
	 */
	protected int m_maxIterations = 1000;
	
	/**
	 * Whether to run the lab on a small set of instances. Used for debugging.
	 */
	boolean m_small = false;

	@SuppressWarnings("unused")
	@Override
	public void setup() 
	{
		// Since loading the specs takes time, show a message 
		System.out.print("Reading specifications, please wait...");
		
		// Experiment factory
		m_factory = new SequenceGenerationExperimentFactory(this);
		//m_factory.add(parseSpecifications("tsg/spec/test", m_small));
		m_factory.add(parseSpecifications("tsg/spec", m_small));
		//m_factory.add(parseSpecifications("tsg/spec/dwyer", m_small));
		//m_factory.add(parseSpecifications("tsg/spec/regex", m_small));
		//m_factory.add(parseSpecifications("tsg/spec/buchistore", m_small));

		// Big region
		CoverageRegion big_r = new CoverageRegion();
		addSpecifications(big_r, m_factory.getSpecificationNames());
		big_r.add(FUNCTION, /*STATE_COVERAGE,*/ TRANSITION_COVERAGE
				//STATE_RESIDUAL, ACTION_COVERAGE, ACTION_RESIDUAL, 
				//TRANSITION_RESIDUAL, TWAY_STATE, TWAY_ACTION, TWAY_TRANSITION
				);
		big_r.add(ALGORITHM, CayleySequenceGenerationExperiment.NAME, 
				//GreedySequenceGenerationExperiment.NAME, 
				//GraphWalkerSequenceGenerationExperiment.NAME,
				DirectSequenceGenerationExperiment.NAME);
		big_r.addRange(STRENGTH, 1, m_maxT);
		big_r.add(CLOSURE, CLOSURE_SPANNING);

		// Analysis of the Cayley method alone
		if (false){
			CoverageRegion cayley_r = new CoverageRegion(big_r);
			cayley_r.set(ALGORITHM, CayleySequenceGenerationExperiment.NAME);
			CayleySizeMacro.CayleyMaxSizeMacro cm_max = new CayleySizeMacro.CayleyMaxSizeMacro(this);
			CayleySizeMacro.CayleyMinSizeMacro cm_min = new CayleySizeMacro.CayleyMinSizeMacro(this);
			add(cm_max, cm_min);
			// Maximum size of specs
			MaxMacro msm_st = new MaxMacro(this, cayley_r.getString(ALGORITHM), "number of states in the specification", STATES);
			add(msm_st);
			MaxMacro msm_tr = new MaxMacro(this, cayley_r.getString(ALGORITHM), "number of transitions in the specification", TRANSITIONS);
			add(msm_tr);
			// Maximum running time
			MaxMacro m_time = new MaxMacro(this, cayley_r.getString(ALGORITHM), "generation time", TIME);
			add(m_time);
			for (Region cm_r : cayley_r.all(FUNCTION, STRENGTH, CLOSURE))
			{
				String latex_function = LatexNamer.latexify(cm_r.getString(FUNCTION));
				String suffix = "";
				if (isCombinatorial(cm_r.getString(FUNCTION)))
				{
					latex_function += LatexNamer.latexify(" t" + cm_r.getInt(STRENGTH));
					suffix = " with t=" + cm_r.getInt(STRENGTH);
				}
				ExperimentTable t_states = new ExperimentTable(STATES, TIME);
				t_states.setNickname("tTimeStates" + LatexNamer.latexify(cm_r.getString(FUNCTION)));
				t_states.setTitle("Generation time with respect to specification size (states) for " + cm_r.getString(FUNCTION) + suffix);
				ExperimentTable t_transitions = new ExperimentTable(TRANSITIONS, TIME);
				t_transitions.setNickname("tTimeTransitions" + latex_function);
				t_transitions.setTitle("Generation time with respect to specification size (transitions) for " + cm_r.getString(FUNCTION) + suffix);
				add(t_states, t_transitions);
				Scatterplot p_states = new Scatterplot(t_states);
				p_states.setNickname("pTimeStates" + latex_function);
				p_states.withLines(false);
				p_states.setCaption(Axis.X, "States").setCaption(Axis.Y, "Time (ms)");
				p_states.setTitle("Generation time with respect to specification size (states) for " + cm_r.getString(FUNCTION) + suffix);
				Scatterplot p_transitions = new Scatterplot(t_transitions);
				p_transitions.setNickname("pTimeTransitions" + latex_function);
				p_transitions.withLines(false);
				p_transitions.setCaption(Axis.X, "Transitions").setCaption(Axis.Y, "Time (ms)");
				p_transitions.setTitle("Generation time with respect to specification size (transitions) for " + cm_r.getString(FUNCTION) + suffix);
				add(p_states, p_transitions);
				for (Region r : cm_r.all(SPECIFICATION))
				{
					CayleySequenceGenerationExperiment<?> sge = (CayleySequenceGenerationExperiment<?>) m_factory.get(r);
					if (sge == null)
					{
						continue;
					}
					t_states.add(sge);
					t_transitions.add(sge);
					cm_min.add(sge);
					cm_max.add(sge);
					msm_st.add(sge);
					msm_tr.add(sge);
					m_time.add(sge);
				}
			}
		}

		// Comparison between Cayley and competition on the same problems
		for (Region cm_r : big_r.all(ALGORITHM))
		{
			String algorithm = cm_r.getString(ALGORITHM);
			if (algorithm.compareTo(CayleySequenceGenerationExperiment.NAME) == 0)
			{
				// We don't compare Cayley to itself ;-)
				continue;
			}
			MinCoverageMacro min_cm = new MinCoverageMacro(this, algorithm);
			AverageCoverageMacro avg_cm = new AverageCoverageMacro(this, algorithm);
			add(min_cm, avg_cm);
			// Test suite size (total length)
			ComparisonTable t_comp = new ComparisonTable(TOTAL_LENGTH, CayleySequenceGenerationExperiment.NAME, cm_r.getString(ALGORITHM));
			t_comp.setTitle("Test suite total length of Cayley vs. " + algorithm);
			t_comp.setNickname("tComp" + LatexNamer.latexify(algorithm));
			add(t_comp);
			Scatterplot vs_plot = new Scatterplot(t_comp);
			vs_plot.setTitle("Test suite total length of Cayley vs. " + algorithm);
			vs_plot.setNickname("pComp" + LatexNamer.latexify(algorithm));
			vs_plot.setCaption(Axis.X, CayleySequenceGenerationExperiment.NAME).setCaption(Axis.Y, cm_r.getString(ALGORITHM));
			vs_plot.withLines(false);
			add(vs_plot);
			ComparisonRatioMacro crm_size = new ComparisonRatioMacro(this, t_comp, algorithm, "total length");
			add(crm_size);
			WeightedComparisonTable t_comp_w = new WeightedComparisonTable(TOTAL_LENGTH, COVERAGE, CayleySequenceGenerationExperiment.NAME, cm_r.getString(ALGORITHM));
			t_comp_w.setTitle("Test suite total length of Cayley vs. " + algorithm);
			t_comp_w.setNickname("tComp" + LatexNamer.latexify(algorithm));
			add(t_comp_w);
			ComparisonRatioMacro crm_w = new ComparisonRatioMacro(this, t_comp_w, algorithm, "weighted total length");
			add(crm_w);
			FullCoverageComparisonRatioMacro fccrm_size = new FullCoverageComparisonRatioMacro(this, t_comp, algorithm, "total length");
			add(fccrm_size);
			// Test suite size (number of sequences)
			ComparisonTable t_comp_nc = new ComparisonTable(SIZE, CayleySequenceGenerationExperiment.NAME, algorithm);
			t_comp_nc.setTitle("Test suite size of Cayley vs. " + algorithm);
			t_comp_nc.setNickname("tCompS" + LatexNamer.latexify(algorithm));
			add(t_comp_nc);
			Scatterplot vs_plot_nc = new Scatterplot(t_comp_nc);
			vs_plot_nc.setTitle("Test suite size of Cayley vs. " + algorithm);
			vs_plot_nc.setNickname("pCompS" + LatexNamer.latexify(algorithm));
			vs_plot_nc.setCaption(Axis.X, CayleySequenceGenerationExperiment.NAME).setCaption(Axis.Y, algorithm);
			vs_plot_nc.withLines(false);
			add(vs_plot_nc);
			ComparisonRatioMacro crm_size_nc = new ComparisonRatioMacro(this, t_comp_nc, algorithm, "number of sequences");
			add(crm_size_nc);
			FullCoverageComparisonRatioMacro fccrm_size_nc = new FullCoverageComparisonRatioMacro(this, t_comp_nc, algorithm, "number of sequences");
			add(fccrm_size_nc);
			// Generation time
			ComparisonTable t_comp_time = new ComparisonTable(TIME, CayleySequenceGenerationExperiment.NAME, algorithm);
			t_comp_time.setTitle("Generation time of Cayley vs. " + algorithm);
			t_comp_time.setNickname("tCompTime" + LatexNamer.latexify(algorithm));
			add(t_comp_time);
			Scatterplot vs_plot_time = new Scatterplot(t_comp_time);
			vs_plot_time.setTitle("Generation time of Cayley vs. " + algorithm);
			vs_plot_time.setNickname("pCompTime" + LatexNamer.latexify(algorithm));
			vs_plot_time.setCaption(Axis.X, CayleySequenceGenerationExperiment.NAME).setCaption(Axis.Y, algorithm);
			vs_plot_time.withLines(false);
			add(vs_plot_time);
			ComparisonRatioMacro crm_time = new ComparisonRatioMacro(this, t_comp_time, algorithm, "time");
			add(crm_time);
			// Maximum number of sequences
			MaxMacro msm = new MaxMacro(this, algorithm, "number of sequences", SIZE);
			add(msm);
			// Incomplete coverage count
			IncompleteCoverageCount m_incomplete = new IncompleteCoverageCount(this, algorithm);
			add(m_incomplete);
			for (Region cm_s : cm_r.all(STRENGTH))
			{
				TCoverageMacro tmc = new TCoverageMacro(this, algorithm, cm_s.getInt(STRENGTH));
				add(tmc);				
				for (Region r_f : cm_s.all(FUNCTION, SPECIFICATION, CLOSURE))
				{
					Region r_cayley = r_f.set(ALGORITHM, CayleySequenceGenerationExperiment.NAME);
					SequenceGenerationExperiment<?> e_cayley = m_factory.get(r_cayley);
					SequenceGenerationExperiment<?> e_other = m_factory.get(r_f);
					if (e_cayley == null || e_other == null)
					{
						continue;
					}
					tmc.add(e_other);
					t_comp.add(e_cayley, e_other);
					t_comp_w.add(e_cayley, e_other);
					t_comp_time.add(e_cayley, e_other);
					t_comp_nc.add(e_cayley, e_other);
					min_cm.add(e_other);
					avg_cm.add(e_other);
					msm.add(e_other);
					m_incomplete.add(e_other);
				}
			}
		}

		// Global macros
		add(new LabStats(this));
		
		// Say we're done
		System.out.println(" done");
	}

	/**
	 * The lab's main loop
	 * @param args Command line arguments
	 */
	public static void main(String[] args)
	{
		initialize(args, MyLaboratory.class);
	}

	/**
	 * Gets the number of specifications used in the lab
	 * @return The number of specifications
	 */
	public int getSpecificationCount()
	{
		return m_factory.getSpecificationNames().size();
	}

	/**
	 * Gets the maximum value of <i>t</i> used in <i>t</i>-way sequence generation
	 * instances
	 * @return The maximum value of <i>t</i>
	 */
	public int getMaxT()
	{
		return m_maxT;
	}

	/**
	 * Gets the maximum number of iterations authorized for the greedy algorithm
	 * @return The number of iterations
	 */
	public int getMaxIterations()
	{
		return m_maxIterations;
	}

	protected static Map<String,String> parseSpecifications(String folder, boolean small)
	{
		int spec_num = 1, spec_cnt = 0;
		Map<String,String> associations = new HashMap<String,String>();
		if (!folder.endsWith("/"))
		{
			folder = folder + "/";
		}
		List<String> file_list = FileHelper.getResourceListing(MyLaboratory.class, folder, ".*\\.(dot|xml|gff)");
		for (String filename : file_list)
		{
			spec_cnt++;
			String full_filename = folder + filename;
			String spec_name = readSpecification(full_filename);
			if (spec_name == null)
			{
				// If no name is given, make one up
				spec_name = "S" + spec_num;
				spec_num++;
			}
			if (small && spec_cnt > 10)
			{
				break;
			}
			associations.put(spec_name, full_filename);
		}
		return associations;
	}

	/**
	 * Reads a specification from an internal file to fetch its name
	 * @param filename The name of the file to read from
	 * @return The name of the specification
	 */
	protected static String readSpecification(String filename)
	{
		if (!filename.startsWith("/"))
		{
			filename = "/" + filename;
		}
		InputStream is = MyLaboratory.class.getResourceAsStream(filename);
		if (is == null)
		{
			return null;
		}
		String name = null;
		if (filename.endsWith("xml") || filename.endsWith("gff"))
		{
			int lastpos = filename.lastIndexOf("/");
			name = "Büchi Store " + filename.substring(lastpos + 1);
			return name.replace(".xml", "").replace(".gff", "");
		}
		Scanner s = new Scanner(is);

		while (s.hasNextLine())
		{
			String line = s.nextLine().trim();
			Matcher mat = s_namePattern.matcher(line);
			if (mat.find())
			{
				name = mat.group(1);
				break;
			}
		}
		s.close();
		return name;
	}

	/**
	 * Adds all specification names to a region
	 * @param r The region
	 * @param names The set of specification names
	 */
	protected static void addSpecifications(Region r, Set<String> names)
	{
		String[] a_names = new String[names.size()];
		int i = 0;
		for (String name : names)
		{
			a_names[i++] = name;
		}
		r.add(SPECIFICATION, a_names);
	}

	/**
	 * Determines whether a triaging function corresponds to a combinatorial
	 * test sequence generation problem, based on its name.
	 * @param function The name of the function
	 * @return <tt>true</tt> if the function is combinatorial, <tt>false</tt>
	 * otherwise
	 */
	protected static boolean isCombinatorial(String function)
	{
		// Simple hack: all these names start with "T-way"
		return function.contains("T-way");
	}

	/**
	 * A region that excludes parameter combinations where the triaging
	 * function is not combinatorial and the strength parameter (i.e. <i>t</i>)
	 * is not 1. This parameter does not have any meaning for non-combinatorial
	 * functions, hence we avoid generating identical experiments that only
	 * differ in their value of <i>t</i>.
	 */
	public static class CoverageRegion extends Region
	{
		public CoverageRegion()
		{
			super();
		}

		public CoverageRegion(Region r)
		{
			super(r);
		}

		@Override
		public boolean isInRegion(Region point)
		{
			if (point.getAll(FUNCTION).size() != 1)
			{
				return true;
			}
			if (!isCombinatorial(point.getString(FUNCTION)) && point.getAll(STRENGTH).size() == 1 && point.getInt(STRENGTH) > 1)
			{
				return false;
			}
			if (point.getString(FUNCTION).contains("residual") && point.getString(CLOSURE).compareTo(CLOSURE_NONE) != 0)
			{
				return false;
			}
			if (!point.getString(FUNCTION).contains("residual") && point.getString(CLOSURE).compareTo(CLOSURE_NONE) == 0)
			{
				return false;
			}
			if (point.getString(ALGORITHM).compareTo(GraphWalkerSequenceGenerationExperiment.ALGORITHM) == 0)
			{
				// GraphWalker does not support some metrics
				if (isCombinatorial(point.getString(FUNCTION)))
				{
					return false;
				}
				if (point.getString(FUNCTION).contains("residual"))
				{
					return false;
				}
				if (point.getString(FUNCTION).contains("Action coverage"))
				{
					return false;
				}
			}
			return true;
		}

		@Override
		protected CoverageRegion getRegion(Region r)
		{
			return new CoverageRegion(r);
		}

		@Override
		public boolean includes(Experiment e)
		{
			return super.includes(e) && (!isCombinatorial(e.readString(ALGORITHM)) || e.hasParameter(STRENGTH));
		}
	}
}
