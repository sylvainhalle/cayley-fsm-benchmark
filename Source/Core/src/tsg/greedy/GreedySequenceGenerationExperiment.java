/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.greedy;

import ca.uqac.lif.ecp.CayleyCategoryCoverage;
import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.CoverageMetric;
import ca.uqac.lif.ecp.PrefixCategoryClosure;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.GreedyAutomatonGenerator;
import ca.uqac.lif.labpal.ExperimentException;
import ca.uqac.lif.labpal.Random;
import tsg.SequenceGenerationExperiment;

/**
 * Generates test sequences by doing a random walk through the specification,
 * favoring edges that have not yet been visited. Note that this experiment
 * still needs a Cayley graph, but it is used only to compute the coverage of
 * the resulting test suite. Therefore, the generation time of the Cayley graph 
 * is not counted in the running time of the experiment.
 * <p>
 * The experiment attempts to generate a test suite multiple times, and keeps
 * the test suite with the highest coverage.
 *
 * @param <U> The output type of the triaging function
 */
public class GreedySequenceGenerationExperiment<U> extends SequenceGenerationExperiment<U>
{
	/**
	 * Name of the algorithm
	 */
	public static final transient String NAME = "SealTest";
	
	/**
	 * The maximum number of iterations
	 */
	protected transient int m_maxIterations = 20;
	
	/**
	 * A random source
	 */
	protected transient Random m_random;
	
	/**
	 * The coverage metric used to compute coverage of the test suite
	 */
	protected transient CoverageMetric<AtomicEvent,Float> m_metric;

	/**
	 * Creates a new empty Cayley-based sequence generation experiment
	 */
	public GreedySequenceGenerationExperiment()
	{
		super();
		setInput(ALGORITHM, NAME);
	}
	
	/**
	 * Sets the random source used by the generator.
	 * @param r The random source
	 * @return This experiment
	 */
	public GreedySequenceGenerationExperiment<U> setRandom(Random r)
	{
		m_random = r;
		return this;
	}

	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		TestSuite<AtomicEvent> suite = null, best_suite = null;
		float best_coverage = 0;
		CayleyGraph<AtomicEvent,U> graph = m_factory.getGraph(m_function);
		if (m_useClosure)
		{
			PrefixCategoryClosure<AtomicEvent,U> closure = new PrefixCategoryClosure<AtomicEvent,U>();
			CayleyGraph<AtomicEvent,U> cl_graph = closure.getClosureGraph(graph);
			m_metric = new CayleyCategoryCoverage<AtomicEvent,U>(cl_graph, m_function);
		}
		else
		{
			m_metric = new CayleyCategoryCoverage<AtomicEvent,U>(graph, m_function);
		}
		long start = System.currentTimeMillis();
		
		for (int it_cnt = 0; it_cnt < m_maxIterations; it_cnt++)
		{
			GreedyAutomatonGenerator generator = new GreedyAutomatonGenerator(m_specification, m_random, m_metric);
			suite = generator.generateTraces();
			float coverage = getCoverage(suite);
			if (coverage > best_coverage)
			{
				best_coverage = coverage;
				best_suite = suite;
			}
			if (coverage == 1 || !isRunning() || System.currentTimeMillis() - start > getMaxDuration())
			{
				break; // Useless to continue: we got full coverage or a timeout
			}
		}
		long end = System.currentTimeMillis();
		writeStats(best_suite);
		write(TIME, end - start);
		write(COVERAGE, best_coverage);
	}
	
	/**
	 * Sets the maximum number of iterations to retry
	 * @param max_iterations The number of iterations
	 */
	public void setMaxIterations(int max_iterations)
	{
		m_maxIterations = max_iterations;
	}
	
	/**
	 * Gets the coverage of a test suite according to the experiment's coverage
	 * metric
	 * @param suite The test suite
	 * @return The coverage
	 */
	protected float getCoverage(TestSuite<AtomicEvent> suite)
	{
		return m_metric.getCoverage(suite); 
	}
}
