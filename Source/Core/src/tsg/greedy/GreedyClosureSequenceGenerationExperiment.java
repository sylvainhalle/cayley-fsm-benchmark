/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.greedy;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.ecp.CayleyCategoryCoverage;
import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.SpanningTreeTraceGenerator;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.Trace;
import ca.uqac.lif.ecp.TraceGenerator;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.GreedyAutomatonGenerator;
import ca.uqac.lif.labpal.ExperimentException;
import ca.uqac.lif.labpal.Random;
import ca.uqac.lif.structures.MathList;
import ca.uqac.lif.structures.MathSet;

/**
 * A {@link GreedySequenceGenerationExperiment} that uses a different technique
 * to compute coverage of the resulting test suite.
 *
 * @param <U> The output type of the triaging function
 */
public class GreedyClosureSequenceGenerationExperiment<U> extends GreedySequenceGenerationExperiment<U>
{
	/**
	 * Creates a new empty Cayley-based sequence generation experiment
	 */
	public GreedyClosureSequenceGenerationExperiment()
	{
		super();
	}
	
	/**
	 * Sets the random source used by the generator.
	 * @param r The random source
	 * @return This experiment
	 */
	public GreedyClosureSequenceGenerationExperiment<U> setRandom(Random r)
	{
		m_random = r;
		return this;
	}

	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		TestSuite<AtomicEvent> suite = null, ref_suite = null, best_suite = null;
		float best_coverage = 0;
		// First, solve the problem using Cayley to get the list of categories that
		// actually occur in the specification. We need this set to correctly calculate
		// the coverage ratio. This part of the processing does not count in the
		// running time.
		CayleyGraph<AtomicEvent,U> graph = m_factory.getGraph(m_function);
		m_metric = new CayleyCategoryCoverage<AtomicEvent,U>(graph, m_function);
		TraceGenerator<AtomicEvent> c_generator = new SpanningTreeTraceGenerator<AtomicEvent,U>(graph);
		ref_suite = c_generator.generateTraces();
		Set<?> all_categories = getCategories(ref_suite);
		int all_categories_n = all_categories.size();
		if (all_categories_n == 0)
		{
			throw new ExperimentException("Error in producing reference set of categories: got an empty set");
		}
		// Now the actual processing can start
		long start = System.currentTimeMillis();
		for (int it_cnt = 0; it_cnt < 1; it_cnt++)
		{
			GreedyAutomatonGenerator generator = new GreedyAutomatonGenerator(m_specification, m_random, m_metric);
			generator.setMaxIterations(m_maxIterations);
			suite = generator.generateTraces();
			Set<?> categories = getCategories(suite);
			float coverage =  (float) categories.size() / (float) all_categories_n;
			if (coverage > 1)
			{
				System.out.println("Got cats: " + categories);
				System.out.println("Suite: " + suite);
				System.out.println("Ref cats: " + all_categories);
				System.out.println("Ref suite: " + ref_suite);
				throw new ExperimentException("Coverage is over 1");
			}
			if (coverage > best_coverage)
			{
				best_coverage = coverage;
				best_suite = suite;
			}
			if (coverage == 1 || !isRunning())
			{
				break; // Useless to continue
			}
		}
		long end = System.currentTimeMillis();
		writeStats(best_suite);
		write(TIME, end - start);
		write(COVERAGE, best_coverage);
	}
	
	/**
	 * Sets the maximum number of iterations to retry
	 * @param max_iterations The number of iterations
	 */
	@Override
	public void setMaxIterations(int max_iterations)
	{
		m_maxIterations = max_iterations;
	}
	
	protected Set<U> getCategories(TestSuite<AtomicEvent> suite)
	{
		int t = readInt(STRENGTH);
		Set<U> categories = new HashSet<U>();
		for (Trace<AtomicEvent> trace : suite)
		{
			m_function.reset();
			addAll(categories, m_function.getStartClass(), t);
			for (AtomicEvent e : trace)
			{
				addAll(categories, m_function.read(e), t);
			}
		}
		return categories;
	}
	
	protected void addAll(Set<U> categories, MathSet<U> cats, int t)
	{
		for (U cat : cats)
		{
			if (!(cat instanceof MathList))
			{
				continue;
			}
			MathList<?> list = (MathList<?>) cat;
			if (list.size() == t)
			{
				categories.addAll(cats);
			}
		}
	}
}
