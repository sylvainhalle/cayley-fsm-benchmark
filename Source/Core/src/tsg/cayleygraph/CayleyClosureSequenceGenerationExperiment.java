/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.cayleygraph;

import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.GreedyTestSuiteFilter;
import ca.uqac.lif.ecp.SpanningTreeTraceGenerator;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.TestSuiteFilter;
import ca.uqac.lif.ecp.TraceGenerator;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.labpal.ExperimentException;

/**
 * Generates test sequences by using the Cayley graph construction.
 *
 * @param <U> The output type of the triaging function
 */
public class CayleyClosureSequenceGenerationExperiment<U> extends CayleySequenceGenerationExperiment<U>
{
	/**
	 * Creates a new empty Cayley-based sequence generation experiment
	 */
	public CayleyClosureSequenceGenerationExperiment()
	{
		super();
	}

	@SuppressWarnings("unchecked")
	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		TestSuite<?> suite = null;
		TestSuiteFilter<AtomicEvent,U> filter = new GreedyTestSuiteFilter<AtomicEvent,U>(m_function);
		long start = System.currentTimeMillis();
		CayleyGraph<AtomicEvent,U> graph = m_factory.getGraph(m_function);
		TraceGenerator<?> generator = null;
		generator = new SpanningTreeTraceGenerator<AtomicEvent,U>(graph);
		write(CAYLEY_STATES, graph.getVertexCount());
		write(CAYLEY_TRANSITIONS, graph.getEdgeCount());
		suite = generator.generateTraces();
		if (!isRunning())
		{
			// Did we time out?
			return;
		}
		suite = filter.filterSuite((TestSuite<AtomicEvent>) suite);
		long end = System.currentTimeMillis();
		writeStats(suite);
		write(TIME, end - start);
		write(COVERAGE, 1); // Cayley experiments have guaranteed full coverage
	}
	
	@Override
	public void useClosure(boolean b)
	{
		// Don't set the m_useClosure field, as it should always be false
		setInput(CLOSURE, CLOSURE_SPANNING);
	}
}
