/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.cayleygraph;

import ca.uqac.lif.ecp.CayleyGraph;
import ca.uqac.lif.ecp.HypergraphTraceGenerator;
import ca.uqac.lif.ecp.PrefixCategoryClosure;
import ca.uqac.lif.ecp.SpanningTreeTraceGenerator;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.TraceGenerator;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.labpal.ExperimentException;
import tsg.SequenceGenerationExperiment;

/**
 * Generates test sequences by using the Cayley graph construction.
 *
 * @param <U> The output type of the triaging function
 */
public class CayleySequenceGenerationExperiment<U> extends SequenceGenerationExperiment<U>
{
	/**
	 * Name of the algorithm
	 */
	public static final transient String NAME = "Cayley";
	
	/**
	 * Name of parameter "Cayley states"
	 */
	public static final transient String CAYLEY_STATES = "Cayley states";
	
	/**
	 * Name of parameter "Cayley transitions"
	 */
	public static final transient String CAYLEY_TRANSITIONS = "Cayley transitions";

	/**
	 * Creates a new empty Cayley-based sequence generation experiment
	 */
	public CayleySequenceGenerationExperiment()
	{
		super();
		setInput(ALGORITHM, NAME);
		describe(CAYLEY_STATES, "The number of states in the resulting Cayley graph");
		describe(CAYLEY_TRANSITIONS, "The number of transitions in the resulting Cayley graph");
	}

	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		TestSuite<?> suite = null;
		long start = System.currentTimeMillis();
		CayleyGraph<AtomicEvent,U> graph = m_factory.getGraph(m_function);
		TraceGenerator<?> generator = null;
		if (m_useClosure)
		{
			PrefixCategoryClosure<AtomicEvent,U> closure = new PrefixCategoryClosure<AtomicEvent,U>();
			graph = closure.getClosureGraph(graph);
			generator = new HypergraphTraceGenerator<AtomicEvent,U>(graph);
		}
		else
		{
			generator = new SpanningTreeTraceGenerator<AtomicEvent,U>(graph);
		}
		if (!isRunning())
		{
			// Did we time out?
			return;
		}
		write(CAYLEY_STATES, graph.getVertexCount());
		write(CAYLEY_TRANSITIONS, graph.getEdgeCount());
		suite = generator.generateTraces();
		long end = System.currentTimeMillis();
		writeStats(suite);
		write(TIME, end - start);
		write(COVERAGE, 1); // Cayley experiments have guaranteed full coverage
	}
	
	@Override
	public void validate()
	{
		super.validate();
		if (readInt(CAYLEY_STATES) <= 1)
		{
			addWarning("The Cayley graph has a suspicious size of " + readInt(CAYLEY_STATES));
		}
	}
}
