/*
    Log trace triaging and etc.
    Copyright (C) 2016 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU Lesser General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU Lesser General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.direct;

import java.util.HashSet;
import java.util.Set;

import ca.uqac.lif.ecp.Edge;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.Trace;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.graphs.LabelledGraph;
import ca.uqac.lif.ecp.graphs.Vertex;
import tsg.SequenceGenerationExperiment;

/**
 * Generates sequences based on an automaton specification, using a "direct"
 * approach. Depending on the coverage metric to be achieved, the "direct"
 * algorithm varies.
 * <ul>
 * <li>For state coverage, the experiment generates a test suite based on
 * the minimum spanning tree of the automaton.</li>
 * <li>For transition coverage, the experiment generates a test suite based
 * on the minimum spanning tree of the line graph built from the
 * automaton.</li>
 * </ul>
 * <p>No other coverage criterion is supported by this experiment.</p>
 * @author Sylvain Hallé
 *
 * @param <U> The type of the categories for the associated coverage metric
 */
public abstract class DirectSequenceGenerationExperiment<U> extends SequenceGenerationExperiment<U>
{
	/**
	 * Name of the algorithm
	 */
	public static final transient String NAME = "Direct";

	/**
	 * Creates a new instance of the experiment.
	 */
	public DirectSequenceGenerationExperiment()
	{
		super();
		setInput(ALGORITHM, NAME);
	}
	
	protected static TestSuite<AtomicEvent> getSuiteFromTraversal(LabelledGraph<AtomicEvent> graph)
	{
		Set<HistoryPair> pairs = new HashSet<HistoryPair>();
		Set<Vertex<AtomicEvent>> visited = new HashSet<Vertex<AtomicEvent>>();
		Set<HistoryPair> to_visit = new HashSet<HistoryPair>();
		TestSuite<AtomicEvent> suite = new TestSuite<AtomicEvent>();
		{
			HistoryPair hp = new HistoryPair();
			hp.current = graph.getInitialVertex();
			hp.history = new Trace<AtomicEvent>();
			to_visit.add(hp);
		}
		traverse(graph, to_visit, visited, pairs);
		for (HistoryPair hp : pairs)
		{
			suite.add(hp.history);
		}
		return suite;
	}

	/**
	 * Performs a breadth-first traversal of a graph.
	 * @param graph
	 * @param to_visit
	 * @param visited
	 * @param pairs
	 */
	protected static void traverse(LabelledGraph<AtomicEvent> graph, Set<HistoryPair> to_visit, Set<Vertex<AtomicEvent>> visited, Set<HistoryPair> pairs)
	{
		while (!to_visit.isEmpty())
		{
			HistoryPair chosen = null;
			for (HistoryPair hp : to_visit)
			{
				if (chosen == null || hp.history.size() < chosen.history.size())
				{
					chosen = hp;
				}
			}
			to_visit.remove(chosen);
			boolean extended = false;
			for (Edge<AtomicEvent> e : chosen.current.getEdges())
			{
				Vertex<AtomicEvent> v = graph.getVertex(e.getDestination());
				if (visited.contains(v))
				{
					continue;
				}
				extended = true;
				visited.add(v);
				HistoryPair hp_new = new HistoryPair();
				hp_new.history = new Trace<AtomicEvent>(chosen.history);
				hp_new.history.add(e.getLabel());
				hp_new.current = graph.getVertex(e.getDestination());
				to_visit.add(hp_new);
			}
			if (!extended)
			{
				pairs.add(chosen);
		}
	}
}

protected static class HistoryPair
{
	Trace<AtomicEvent> history;

	Vertex<AtomicEvent> current;
	
	@Override
	public String toString()
	{
		return history + "(" + current + ")";
	}
}
}
