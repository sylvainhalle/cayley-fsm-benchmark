/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.direct;

import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import ca.uqac.lif.ecp.Edge;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.Trace;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.ecp.graphs.ForestNode;
import ca.uqac.lif.ecp.graphs.LabelledGraph;
import ca.uqac.lif.ecp.graphs.Vertex;
import ca.uqac.lif.labpal.ExperimentException;
import ca.uqac.lif.structures.MathList;

/**
 * Generates a set of test sequences that achieve full transition coverage
 * using the minimum spanning tree of the
 * <a href="https://reference.wolfram.com/language/ref/LineGraph.html">line graph</a>
 * of the input specification.
 * @author Sylvain Hallé
 */
public class LineGraphSequenceGenerationExperiment extends DirectSequenceGenerationExperiment<MathList<Edge<AtomicEvent>>>
{
	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		long start_time = System.currentTimeMillis();
		LabelledGraph<AtomicEvent> g = getLineGraph(m_specification);
		TestSuite<AtomicEvent> suite = getSuiteFromTraversal(g);
		long end_time = System.currentTimeMillis();
		write(TIME, end_time - start_time);
		writeStats(suite);
	}

	/**
	 * Computes the minimum spanning tree of the automaton.
	 * @param aut The automaton
	 * @return Another automaton with transitions removed, corresponding to the
	 * minimum spanning tree
	 */
	protected static LabelledGraph<AtomicEvent> getMinimumSpanningTree(LabelledGraph<AtomicEvent> aut)
	{
		ForestNode<AtomicEvent> forest = new ForestNode<AtomicEvent>(null);
		List<Edge<AtomicEvent>> edges = forest.getOrderedSpanningTree(aut);
		LabelledGraph<AtomicEvent> tree = new LabelledGraph<AtomicEvent>(aut);
		for (Vertex<AtomicEvent> v : tree.getVertices())
		{
			Iterator<Edge<AtomicEvent>> e_it = v.getEdges().iterator();
			while (e_it.hasNext())
			{
				Edge<AtomicEvent> e = e_it.next();
				if (!edges.contains(e))
				{
					// This edge is not in the list; take it off
					e_it.remove();
				}
			}
		}
		tree.setInitialVertexId(aut.getInitialVertex().getId());
		return tree;
	}

	/**
	 * Generates a set of traces from a spanning tree.
	 * @param tree The tree
	 * @param current The current node in the tree
	 * @param history The trace of all states visited from the tree's root
	 * @param suite A set where traces are added. When the top-level call
	 * of this method returns, the test suite is inside this set.
	 */
	protected static void generateTraces(LabelledGraph<AtomicEvent> tree, Vertex<AtomicEvent> current, Trace<AtomicEvent> history, Set<Trace<AtomicEvent>> suite)
	{
		Set<Edge<AtomicEvent>> edges = current.getEdges();
		if (edges.isEmpty())
		{
			// Leaf of the tree, add the history trace to the test suite
			suite.add(history);
			return;
		}
		for (Edge<AtomicEvent> e : current.getEdges())
		{
			int dst_id = e.getDestination();
			Trace<AtomicEvent> sub_t = new Trace<AtomicEvent>(history); // Creates a copy
			sub_t.add(e.getLabel());
			generateTraces(tree, tree.getVertex(dst_id), sub_t, suite);
		}
	}

	/**
	 * Computes the line graph of the automaton.
	 * @param aut The automaton
	 * @return The graph corresponding to the line graph of <tt>aut</tt>
	 */
	protected static LabelledGraph<AtomicEvent> getLineGraph(Automaton aut)
	{
		List<Edge<AtomicEvent>> a_edges = aut.getEdges();
		LabelledGraph<AtomicEvent> lg = new LabelledGraph<AtomicEvent>();
		Map<Edge<AtomicEvent>,Integer> states = new HashMap<Edge<AtomicEvent>,Integer>(a_edges.size());
		for (int i = 0; i < a_edges.size(); i++)
		{
			Edge<AtomicEvent> a_e = a_edges.get(i);
			states.put(a_e, i + 1);
		}
		Vertex<AtomicEvent> lg_init = new Vertex<AtomicEvent>(0);
		for (Map.Entry<Edge<AtomicEvent>,Integer> me1 : states.entrySet())
		{
			Edge<AtomicEvent> init = me1.getKey();
			Vertex<AtomicEvent> v = new Vertex<AtomicEvent>(me1.getValue());
			for (Map.Entry<Edge<AtomicEvent>,Integer> me2 : states.entrySet())
			{
				Edge<AtomicEvent> dest = me2.getKey();
				if (init.getDestination() == dest.getSource())
				{
					v.add(new Edge<AtomicEvent>(me1.getValue(), dest.getLabel(), me2.getValue()));
				}
			}
			lg.add(v);
			if (init.getSource() == aut.getInitialVertex().getId())
			{
				lg_init.add(new Edge<AtomicEvent>(0, init.getLabel(), me1.getValue()));
			}
		}
		lg.add(lg_init);
		lg.setInitialVertexId(0);
		return lg;
	}

	public static void main(String[] args)
	{
		Automaton g = new Automaton();
		{
			Vertex<AtomicEvent> v = new Vertex(1);
			v.add(new Edge<AtomicEvent>(1, new AtomicEvent("a"), 2));
			v.add(new Edge<AtomicEvent>(1, new AtomicEvent("b"), 3));
			v.add(new Edge<AtomicEvent>(1, new AtomicEvent("c"), 4));
			g.add(v);
		}
		{
			Vertex<AtomicEvent> v = new Vertex(2);
			v.add(new Edge<AtomicEvent>(2, new AtomicEvent("d"), 3));
			g.add(v);
		}
		g.setInitialVertexId(1);
		LabelledGraph<AtomicEvent> lg = getLineGraph(g);
	}
}
