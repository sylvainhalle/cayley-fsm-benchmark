/*
    A benchmark for Cayley graph test sequence generation
    Copyright (C) 2020 Sylvain Hallé

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published
    by the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Lesser General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
package tsg.direct;

import java.util.Iterator;
import java.util.List;
import java.util.Set;

import ca.uqac.lif.ecp.Edge;
import ca.uqac.lif.ecp.TestSuite;
import ca.uqac.lif.ecp.Trace;
import ca.uqac.lif.ecp.atomic.AtomicEvent;
import ca.uqac.lif.ecp.atomic.Automaton;
import ca.uqac.lif.ecp.graphs.ForestNode;
import ca.uqac.lif.ecp.graphs.Vertex;
import ca.uqac.lif.labpal.ExperimentException;
import ca.uqac.lif.structures.MathList;

/**
 * Generates a set of test sequences that achieve full state coverage
 * using the minimum spanning tree of the input specification.
 * @author Sylvain Hallé
 */
public class SpanningTreeSequenceGenerationExperiment extends DirectSequenceGenerationExperiment<MathList<Integer>>
{
	@Override
	public void execute() throws ExperimentException, InterruptedException
	{
		long start_time = System.currentTimeMillis();
		TestSuite<AtomicEvent> suite = getSuiteFromTraversal(m_specification);
		long end_time = System.currentTimeMillis();
		write(TIME, end_time - start_time);
		write(COVERAGE, 1); // Full coverage is guaranteed
		writeStats(suite);
	}
	
	protected static Automaton getMinimumSpanningTree(Automaton aut)
	{
		ForestNode<AtomicEvent> forest = new ForestNode<AtomicEvent>(null);
		List<Edge<AtomicEvent>> edges = forest.getOrderedSpanningTree(aut);
		Automaton tree = new Automaton(aut);
		for (Vertex<AtomicEvent> v : tree.getVertices())
		{
			Iterator<Edge<AtomicEvent>> e_it = v.getEdges().iterator();
			while (e_it.hasNext())
			{
				Edge<AtomicEvent> e = e_it.next();
				if (!edges.contains(e))
				{
					// This edge is not in the list; take it off
					e_it.remove();
				}
			}
		}
		tree.setInitialVertexId(aut.getInitialVertex().getId());
		return tree;
	}
	
	/**
	 * Generates a set of traces from a spanning tree
	 * @param tree The tree
	 * @param current The current node in the tree
	 * @param history The trace of all states visited from the tree's root
	 * @param suite A set where traces are added. When the top-level call
	 * of this method returns, the test suite is inside this set.
	 */
	protected static void generateTraces(Automaton tree, Vertex<AtomicEvent> current, Trace<AtomicEvent> history, Set<Trace<AtomicEvent>> suite)
	{
		Set<Edge<AtomicEvent>> edges = current.getEdges();
		if (edges.isEmpty())
		{
			// Leaf of the tree, add the history trace to the test suite
			suite.add(history);
			return;
		}
		for (Edge<AtomicEvent> e : current.getEdges())
		{
			int dst_id = e.getDestination();
			Trace<AtomicEvent> sub_t = new Trace<AtomicEvent>(history); // Creates a copy
			sub_t.add(e.getLabel());
			generateTraces(tree, tree.getVertex(dst_id), sub_t, suite);
		}
	}
}
